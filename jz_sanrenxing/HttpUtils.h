//
//  HttpUtils.h
//  jz_sanrenxing
//
//  Created by Killua on 2016/10/5.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^CallbackArray)(int code, NSArray *array, NSString *msg);
typedef void (^CallbackDictionary)(int code, NSDictionary *dict, NSString *msg);
typedef void(^CallbackNoData)(int code, NSString *msg);

@interface HttpUtils : NSObject


/**
 获取验证码

 @param phone    手机号码
 @param callback 请求回调
 */
+ (void)requestVerificationCodeWithPhone:(NSString *)phone callback:(CallbackNoData)callback;


/**
 用户注册

 @param phone    手机号码
 @param password 密码
 @param vCode    验证码
 @param callback 请求回调
 */
+ (void)requestToRegisterWithPhone:(NSString *)phone password:(NSString *)password vCode:(NSString *)vCode callback:(CallbackDictionary)callback;



/**
 用户登录

 @param phone    手机号码
 @param password 密码
 @param callback 请求回调
 */
+ (void)requestToLoginWithPhone:(NSString *)phone password:(NSString *)password callback:(CallbackDictionary)callback;



/**
 修改用户信息

 @param nickName 昵称
 @param email    邮箱
 @param phone    手机号码
 @param name     姓名
 @param fileURL 文件路径URL
 @param gender   性别
 @param birthday 生日
 @param brief    简介
 @param careers  职业
 @param callback 请求回调
 */
+ (void)requestToUpdateUserInfoWithNickName:(NSString *)nickName
                                      email:(NSString *)email
                                      phone:(NSString *)phone
                                       name:(NSString *)name
                                   fileURL:(NSURL *)fileURL
                                     gender:(NSString *)gender
                                   birthday:(NSString *)birthday
                                      brief:(NSString *)brief
                                    careers:(NSString *)careers
                                   callback:(CallbackDictionary)callback;

/**
 首页接口
 
 @param          nil
 */

+ (void)requestIndexDatacallback:(CallbackDictionary)callback;




/**
 老师首页接口
 
 @param          nil
 */

+ (void)requestTeacherDatacallback:(CallbackDictionary)callback;





/**
 发布课程接口
 
 @param course.user_id        用户ID
 @param course.course_name    课程名称
 @param course.course_brief   课程简介
 @param course.course_keyword 课程关键字
 @param coverAvatar           课程封面
 @param course.course_property课程性质
 @param course.course_pay     费用
 @param course.course_part_num章节总数
 @param course.course_bdate   开课日期
 @param course.course_edate   结束日期
 @param course.course_time    开讲时间
 @param course.is_hot         是否热门
 @param course.is_recommend   是否推荐
 @param crowdfunding.crowdfunding_target_num  众筹目标人数
 @param crowdfunding.crowdfunding_target_money众筹目标金额
 @param crowdfunding.crowdfunding_bdate       众筹开始时间
 @param crowdfunding.crowdfunding_edate       众筹结束时间
 @param token                 用户token
 */
+ (void)requestToAddCourseWithCourse_name:(NSString *)course_name
                                     course_brief:(NSString *)course_brief
                                   course_keyword:(NSString *)course_keyword
                                      coverAvatar:(NSURL    *)coverAvatar
                                  course_property:(NSString *)course_property
                                       course_pay:(double    )course_pay
                                  course_part_num:(int       )course_part_num
                                     course_bdate:(NSString *)course_bdate
                                     course_edate:(NSString *)course_edate
                                      course_time:(NSString *)course_time
                                           is_hot:(NSString *)is_hot
                                     is_recommend:(NSString *)is_recommend
                          crowdfunding_target_num:(int       )crowdfunding_target_num
                        crowdfunding_target_money:(double    )crowdfunding_target_money
                                         callback:(CallbackDictionary)callback;





/**
 课程列表接口
 
 @param course.course_id      课程ID
 @param course.user_id        所属用户
 @param course.type_id        课程类型
 @param course.course_name    课程名
 @param course.course_keyword 课程关键字
 @param course.course_property课程性质
 @param course.course_status  课程状态
 @param orderBy               排序方式
 @param course.is_hot         是否热门
 @param course.is_recommend   是否推荐
 
 */
+ (void)requestMyCourseWithPage:(int )page pagesize:(int )pagesize callback:(CallbackDictionary)callback;







/**
 发布章节接口
 
 @param part.course_id        课程ID
 @param part.part_name        章节名称
 @param part.part_url         章节视频链接
 @param part.part_brief       章节简介
 @param part.part_keyword     章节关键字
 
 */
+ (void)UploadCapterWithPart_name:(NSString *)part_name
                                course_id:(NSString *)course_id
                                 part_url:(NSURL    *)part_url
                               part_brief:(NSString *)part_brief
                             part_keyword:(NSString *)part_keyword
                                 callback:(CallbackDictionary)callback;






/**
 获取视频URL的接口（基于优酷的三方接口）
 
 @param client_id             应用key
 @param video_id              视频id
 
 */

+ (void)requestVideoURLWithVideoID:(NSString *)video_id client_id:(NSString *)client_id callback:(CallbackDictionary)callback ;



/**
 章节列表接口
 
 @param part.course_id        课程id
 @param orderBy               排序方式
 @param page                  第几页
 @param pagesize              每页数据条数
 
 */
+ (void)requestMyCapterListWithPage:(int )page pagesize:(int )pagesize courseID:(NSString *)course_id callback:(CallbackDictionary)callback;




/**
 删除课程接口
 
 @param course.course_id      课程id
 @param token                 用户token
 
 */
+ (void)requestDeleteCoureseWithCourseID:(NSString *)course_id callback:(CallbackDictionary)callback;

@end
