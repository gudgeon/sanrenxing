//
//  AppDelegate.h
//  jz_sanrenxing
//
//  Created by GuJun on 16/9/8.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JZTabBarController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)loadMainVC;

@end

