//
//  GuideViewController.m
//  jz_sanrenxing
//
//  Created by Killua on 2016/10/5.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "GuideViewController.h"
#import "AppDelegate.h"

#define KEY_GUIDE_MAX 3
#define KEY_GUIDE_MIN 1

@interface GuideViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *guideImageView;
@property (nonatomic) int imageIndex;

@end

@implementation GuideViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _imageIndex = 1;
    [self addGesture];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)addGesture {
    UISwipeGestureRecognizer *left = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
    left.direction = UISwipeGestureRecognizerDirectionLeft;

    UISwipeGestureRecognizer *right = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
    right.direction = UISwipeGestureRecognizerDirectionRight;

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];

    [_guideImageView addGestureRecognizer:left];
    [_guideImageView addGestureRecognizer:right];
    [_guideImageView addGestureRecognizer:tap];
}

- (void)swipe:(UISwipeGestureRecognizer *)sender {
    if (sender.direction == UISwipeGestureRecognizerDirectionLeft) {
        if (_imageIndex <= KEY_GUIDE_MAX) {
            _imageIndex += 1;
        }
    } else if (sender.direction == UISwipeGestureRecognizerDirectionRight) {
        if (_imageIndex > KEY_GUIDE_MIN) {
            _imageIndex -= 1;
        }
    }

    if (_imageIndex < KEY_GUIDE_MAX + 1) {
        NSString *imageName = [NSString stringWithFormat:@"guide%d", _imageIndex];
        _guideImageView.image = [UIImage imageNamed:imageName];
    } else {
        // 进入主页面
        [self showMainVC];
    }
}

- (void)tap:(UITapGestureRecognizer *)sender {
    if (_imageIndex == KEY_GUIDE_MAX) {
        [self showMainVC];
    }
}

- (void)showMainVC {
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [delegate loadMainVC];
}
@end
