//
//  ImageSelect.h
//  jz_sanrenxing
//
//  Created by Killua on 2016/10/6.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ImageSelectDelegate <NSObject>

@optional
- (void)selectedImage:(UIImage *)image FullPath:(NSString *)fullPath VideoPath:(NSString *)videoPath;

@end

@interface ImageSelectManager : NSObject

@property(nonatomic,strong)NSString *fullPath;
@property (nonatomic, weak) id<ImageSelectDelegate> delegate;

- (void)pickImageFromCamera:(BOOL)editing ;
- (void)pickImageFromAlbum:(BOOL)editing;
- (void)pickVideoFromAlbum:(BOOL)editing;
- (UIImage*)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize;

@end
