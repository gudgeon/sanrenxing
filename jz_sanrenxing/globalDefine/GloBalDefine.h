//
//  GloBalDefine.h
//  jz_sanrenxing
//
//  Created by GuJun on 16/9/8.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#ifndef GloBalDefine_h
#define GloBalDefine_h

#import "URLDefine.h"


//颜色设置简化
#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]


//第三方库
#define MAS_SHORTHAND
#define MAS_SHORTHAND_GLOBALS
#import <Masonry.h>
#import <SDCycleScrollView.h>
#import <SDWebImage/UIImageView+WebCache.h>


//屏幕的宽和高
#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

#define isIPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size)) : NO)

#define isIPhone6plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(1125, 2001), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size)) : NO)

//将其他类型转换成字符串
#import "NSStringTool.h"
#define judgeStr(str)  [NSStringTool judgeStr:str]

//json转模型
#import "MJExtension.h"

//刷新
#import <MJRefresh.h>

//弹框
//#import "iToast.h"


#endif /* GloBalDefine_h */
