//
//  URLDefine.h
//  jz_sanrenxing
//
//  Created by GuJun on 16/9/8.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#ifndef URLDefine_h
#define URLDefine_h

#define HOST @"http://jindra.wicp.net/HeiNiuEducation/app/"

#define SYSTEM_ERROR 9999
#define KEY_CLIENT_TYPE @"2"
#define UUID [[NSUUID UUID].UUIDString stringByReplacingOccurrencesOfString:@"-" withString:@""]


// 接口
// 获取验证码
#define verificationCode @"user!getRegistCode.action"

// 注册
#define userRegister @"user!register.action"

// 登录
#define userLogin @"user!clientLogin.action"

// 首页数据接口
#define CourseList    @"course!getCourseHomeList.action"

// 修改用户信息
#define updateUserInfo @"user!updateUser.action"

// 老师首页接口
#define getTeacherInfo    @"user!getTeacherHomeList.action"

// 发布课程
#define addCourse   @"course!addCourse.action"

// 获取课程列表
#define getCourseList     @"course!getCourseList.action"

// 发布章节
#define uploadCapter         @"part!addCoursePart.action"

// 章节列表接口
#define getCapterList        @"part!getCoursePartList.action"

// 删除课程
#define deleteCourse         @"course!deleteCourse.action"

#endif /* URLDefine_h */
