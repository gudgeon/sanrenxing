//
//  NSStringTool.h
//  AiShangFuYang
//
//  Created by 周德江 on 16/5/3.
//  Copyright © 2016年 jingzhong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSStringTool : NSObject


/**
 *  字符串格式化null自动转为@""
 *
 *  @param str 任意类型的变量
 *
 *  @return 返回一个字符串
 */
+ (NSString *)judgeStr:(id)str;

//判断字符串是否是null
+ (BOOL)isNullString:(NSString *)string;

@end
