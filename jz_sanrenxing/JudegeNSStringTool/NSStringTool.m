//
//  NSStringTool.m
//
//
//  Created by 周德江 on 16/5/3.
//  Copyright © 2016年 jingzhong. All rights reserved.
//

#import "NSStringTool.h"

@implementation NSStringTool


+ (NSString *)judgeStr:(id)str
{
    NSString *str1 = [NSString stringWithFormat:@"%@",str];
    if ([str1 isEqualToString:@"(null)"]) {
        return @"";
    }
    else
    {
        return [NSStringTool isNullString:str1] != 0?@"":str1;
    }
}

+ (BOOL)isNullString:(NSString *)string
{
    if(string == nil){
        return YES;
    }
    
    if ((NSNull *)string == [NSNull null]) {
        return YES;
    }
    
    string = [string stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if([string length] == 0){
        return YES;
    }
    return NO;
}


@end
