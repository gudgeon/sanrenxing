//
//  UIColor+ZXLazy.h
//  jz_sanrenxing
//
//  Created by GuJun on 16/10/9.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ZXLazy)
+ (UIColor *)colorWithRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue;

/**
 *  16进制转uicolor
 *
 *  @param color @"#FFFFFF" ,@"OXFFFFFF" ,@"FFFFFF"
 *
 *  @return uicolor
 */
+ (UIColor *)colorWithHexString:(NSString *)color;

@end
