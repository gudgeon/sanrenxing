//
//  CALayer+XibConfiguration.h
//  jz_sanrenxing
//
//  Created by GuJun on 16/10/9.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CALayer (XibConfiguration)

@property(nonatomic, assign) UIColor *borderUIColor;

@end
