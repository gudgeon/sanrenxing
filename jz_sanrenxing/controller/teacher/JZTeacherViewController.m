//
//  JZTeacherViewController.m
//  jz_sanrenxing
//
//  Created by GuJun on 16/9/8.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "JZTeacherViewController.h"
#import "TeacherCollectionViewCell.h"
#import "JZTeacherCollectionReusableView.h"
#import "TeacherHomeCVL.h"
#import "TeacherNormalCRV.h"
#import "TeacherHotCRV.h"
#import "TeacherFootCRV.h"
#import "JZRaiseModel.h"
#import "JZWebViewController.h"

@interface JZTeacherViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>{
    NSInteger sectionNum;
}

@property (nonatomic, strong) UICollectionView   *raiseCollectionView;

/* 正在众筹（热门）   **/
@property (nonatomic, strong) NSMutableArray     *hotMutableArray;

/* 推荐            **/
@property (nonatomic, strong) NSMutableArray     *recommendMutableArray;

@end

@implementation JZTeacherViewController

static NSString * const reuseIdentifier = @"raiseCollectionCell";
static NSString *normalHeadViewIdentifier = @"normalHeadViewIdentifier";
static NSString *hotHeadViewIdentifier = @"hotHeadViewIdentifier";
static NSString *footViewIdentifier = @"footViewIdentifier";


- (void)viewDidLoad {
    [super viewDidLoad];

    //布局collection
    [self drawRaiseCollectionView];

    [self refreshToRequestData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [_raiseCollectionView.mj_header beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)drawRaiseCollectionView{
    TeacherHomeCVL *layout = [TeacherHomeCVL teacherInfoLayout];
    _raiseCollectionView =[[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
    _raiseCollectionView.collectionViewLayout = layout;
    _raiseCollectionView.backgroundColor = [UIColor lightGrayColor];
    _raiseCollectionView.alwaysBounceVertical = YES;
    _raiseCollectionView.backgroundColor = [UIColor whiteColor];
    _raiseCollectionView.delegate = self;
    _raiseCollectionView.dataSource = self;
    [self.view addSubview:_raiseCollectionView];

    [_raiseCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(self.view);
        make.height.mas_equalTo(self.view);
        make.center.mas_equalTo(self.view);
    }];

    // 注册cell
    [_raiseCollectionView registerClass:[TeacherCollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];

    // head view
    [_raiseCollectionView registerClass:[TeacherNormalCRV class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:normalHeadViewIdentifier];
    [_raiseCollectionView registerClass:[TeacherHotCRV class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:hotHeadViewIdentifier];

    // footView
    [_raiseCollectionView registerClass:[TeacherFootCRV class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:footViewIdentifier];
}

- (void)refreshToRequestData {

    sectionNum = 0;

    // 下拉刷新
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    _raiseCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    
    // 进入刷新状态
    [_raiseCollectionView.mj_header beginRefreshing];
    
}

- (void)loadNewData{
    //结束上拉刷新
    [self.raiseCollectionView.mj_footer endRefreshing];
    
    __weak typeof(self) weakSelf=self;
    
    [HttpUtils requestTeacherDatacallback:^(int code, NSDictionary *dict, NSString *msg) {
        if (1 == code) {
            sectionNum = 2;

            // 字典 -> 模型
            weakSelf.hotMutableArray = [JZRaiseModel mj_objectArrayWithKeyValuesArray:dict[@"list"][@"hotList"]];
            weakSelf.recommendMutableArray = [JZRaiseModel mj_objectArrayWithKeyValuesArray:dict[@"list"][@"recommendList"]];

            //刷新tableView
            [weakSelf.raiseCollectionView reloadData];
            
            // 结束刷新
            [weakSelf.raiseCollectionView.mj_header endRefreshing];
            
        } else {
            [SVProgressHUD showInfoWithStatus:msg];
            [weakSelf.raiseCollectionView.mj_header endRefreshing];
        }
        
    }];

}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return sectionNum;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 0) {
        return self.hotMutableArray.count;
    }else{
        return self.recommendMutableArray.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    TeacherCollectionViewCell *cell = (TeacherCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];

    JZRaiseModel *hotRaiseModel = self.hotMutableArray[indexPath.row];
    JZRaiseModel *recommendRaiseModel = self.recommendMutableArray[indexPath.row];
    
    switch (indexPath.section) {
            //热门
        case 0:

            cell.raiseUserDetailModel = hotRaiseModel.userDetail;
            break;
            
            //推荐
        case 1:
            cell.raiseUserDetailModel = recommendRaiseModel.userDetail;
            break;
            
        default:
            break;
    }

    return cell;
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if (kind == UICollectionElementKindSectionHeader) {
        TeacherBaseCollectionReusableView *headView;
        NSInteger section = indexPath.section;
        switch (section) {
            case 0: {
                headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:hotHeadViewIdentifier forIndexPath:indexPath];
                break;
            }
            case 1: {
                headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:normalHeadViewIdentifier forIndexPath:indexPath];
                break;
            }
        }
        return headView;
    } else if (kind == UICollectionElementKindSectionFooter) {
        TeacherFootCRV *footView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:footViewIdentifier forIndexPath:indexPath];
        return footView;
    } else {
        return nil;
    }
}


#pragma mark <UICollectionViewDelegate>
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    JZWebViewController *webviewVC = [JZWebViewController new];
    
        webviewVC.urlStr = @"http://jindra.wicp.net/HeiNiuEducation/jiaoshizl.html";
        
    
    [self.navigationController pushViewController:webviewVC animated:YES];

}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    CGSize size = CGSizeZero;
    switch (section) {
        case 0: {
            size = CGSizeMake(SCREEN_WIDTH, 75);
            break;
        }

        case 1: {
            size = CGSizeMake(SCREEN_WIDTH, 42);
            break;
        }
    }
    return size;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    CGSize size = CGSizeZero;
    if (section == 0) {
        size = CGSizeMake(SCREEN_WIDTH, 10);
    }
    return size;
}

@end
