//
//  JZWebViewController.h
//  jz_sanrenxing
//
//  Created by GuJun on 16/10/8.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JZWebViewController : UIViewController

@property (nonatomic)  NSString   *urlStr;

@end
