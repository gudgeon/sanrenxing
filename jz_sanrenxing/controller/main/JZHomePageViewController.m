//
//  JZHomePageViewController.m
//  jz_sanrenxing
//
//  Created by GuJun on 16/9/29.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "JZHomePageViewController.h"
#import "JZCourseTableViewCell.h"
#import "JZHomePageHeadView.h"
#import "JZCourseModel.h"
#import "JZWebViewController.h"


@interface JZHomePageViewController ()<UITableViewDataSource,UITableViewDelegate>{
    
    NSInteger sectionNum;
    
}

/** section  title array*/
@property (nonatomic, strong) NSArray            *sectiontitleArray;

/**  tableview  */
@property (weak, nonatomic) IBOutlet UITableView *courseTableview;

/**  定价课 array  */
@property (nonatomic, strong) NSMutableArray     *chargeMutableArray;

/**  推荐课 array  */
@property (nonatomic, strong) NSMutableArray     *recommendMutableArray;

/**  众筹课 array  */
@property (nonatomic, strong) NSMutableArray     *crowdfundingMutableArray;

/**  免费课 array  */
@property (nonatomic, strong) NSMutableArray     *freeMutableArray;



@end

@implementation JZHomePageViewController

- (NSArray *)sectiontitleArray{
    
    if (!_sectiontitleArray) {
        _sectiontitleArray = [NSArray arrayWithObjects:@"推荐",@"定价课程",@"免费课程",@"正在众筹", nil];
        //_sectiontitleArray = @[@"推荐",@"定价课程",@"免费课程",@"正在众筹"];
    }
    return _sectiontitleArray;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.courseTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //刷新
    [self RefreshData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 下拉刷新
- (void)RefreshData
{
    sectionNum = 0;

    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.courseTableview.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    
    // 进入刷新状态
    [self.courseTableview.mj_header beginRefreshing];

    
}

- (void)loadNewData
{
    //结束上拉刷新
    [self.courseTableview.mj_footer endRefreshing];
    
    __weak typeof(self) weakSelf=self;
    
    [HttpUtils requestIndexDatacallback:^(int code, NSDictionary *dict, NSString *msg) {
        if (1 == code) {
            sectionNum = 4;
            // 字典 -> 模型
            weakSelf.chargeMutableArray = [JZCourseModel mj_objectArrayWithKeyValuesArray:dict[@"list"][@"chargeCourse"]];
            weakSelf.crowdfundingMutableArray = [JZCourseModel mj_objectArrayWithKeyValuesArray:dict[@"list"][@"crowdfundingCourse"]];
            weakSelf.freeMutableArray = [JZCourseModel mj_objectArrayWithKeyValuesArray:dict[@"list"][@"freeCourse"]];
            weakSelf.recommendMutableArray = [JZCourseModel mj_objectArrayWithKeyValuesArray:dict[@"list"][@"recommendCourse"]];
            
            //刷新tableView
            [weakSelf.courseTableview reloadData];
            
            // 结束刷新
            [weakSelf.courseTableview.mj_header endRefreshing];
            
        } else {
            [SVProgressHUD showInfoWithStatus:msg];
            [weakSelf.courseTableview.mj_header endRefreshing];
        }
        
    }];

}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return sectionNum;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    switch (section) {
        case 0:
            return self.recommendMutableArray.count;
            break;
        case 1:
            return self.chargeMutableArray.count;
            break;
        case 2:
            return self.freeMutableArray.count;
            break;
        case 3:
            return self.crowdfundingMutableArray.count;
            break;
        default:
            return 0;
            break;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    JZCourseTableViewCell *cell = [JZCourseTableViewCell mainTableViewCellWithTableView:tableView];
    
    switch (indexPath.section) {
            //推荐课
        case 0:
            cell.courseModel = self.recommendMutableArray[indexPath.row];
            break;
            
            //定价课
        case 1:
            cell.courseModel = self.chargeMutableArray[indexPath.row];
            break;
            
            //免费课
        case 2:
            cell.courseModel = self.freeMutableArray[indexPath.row];
            break;
            
            //众筹课
        case 3:
            cell.courseModel = self.crowdfundingMutableArray[indexPath.row];
            break;

        default:
            break;
    }
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 212;
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    JZWebViewController *webviewVC = [JZWebViewController new];
    
    if (indexPath.section == 3) {
        
        webviewVC.urlStr = @"http://jindra.wicp.net/HeiNiuEducation/jsxq.html";
        
    }else{
        
        webviewVC.urlStr = @"http://jindra.wicp.net/HeiNiuEducation/jiaoshizl.html";

    }
    
    [self.navigationController pushViewController:webviewVC animated:YES];
    
}

//section底部间距
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 38;
}

//section底部视图
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    UIView *view=[[UIView alloc] init];
    
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, 10);
    view.backgroundColor = RGBCOLOR(240, 240, 240);
    
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

    JZHomePageHeadView *headView = [[JZHomePageHeadView alloc] init];
    
    headView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 38);
    
    headView.backgroundColor = RGBCOLOR(240, 240, 240);

    headView.titleLabel.text = self.sectiontitleArray[section];
    
    return headView;
    
}


@end
