//
//  JZCourseCatalogViewController.m
//  jz_sanrenxing
//
//  Created by GuJun on 16/10/9.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "JZCourseCatalogViewController.h"
#import "JZReleaseCapterViewController.h"
#import "JZCourseCatalogCell.h"
#import "JZCapterModel.h"

@interface JZCourseCatalogViewController ()

/** 我的课程章节数据数组 */
@property (nonatomic, strong) NSMutableArray   *myCapterMutableArray;


@end

@implementation JZCourseCatalogViewController

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    //刷新
    [self RefreshData];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"课程目录";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"myCapter_add"] style:UIBarButtonItemStylePlain target:self action:@selector(addChapter)];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 下拉刷新 上拉加载
- (void)RefreshData
{
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    
    // 进入刷新状态
    [self.tableView.mj_header beginRefreshing];
    
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    
    
}
//下拉刷新
- (void)loadNewData{
    
    //结束上拉刷新
    [self.tableView.mj_footer endRefreshing];
    
    //发送请求
    __weak typeof(self) weakSelf=self;
    
    [HttpUtils requestMyCapterListWithPage:1 pagesize:10 courseID:self.courseModel.course_id callback:^(int code, NSDictionary *dict, NSString *msg) {
        if (1 == code) {
            //            // 字典 -> 模型
            weakSelf.myCapterMutableArray = [JZCapterModel mj_objectArrayWithKeyValuesArray:dict[@"list"]];
            //
            //刷新tableView
            [weakSelf.tableView reloadData];
            
            // 结束刷新
            [weakSelf.tableView.mj_header endRefreshing];
            
        } else {
            [SVProgressHUD showInfoWithStatus:msg];
            [weakSelf.tableView.mj_header endRefreshing];
        }
        
    }];
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.myCapterMutableArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    JZCourseCatalogCell *cell = [JZCourseCatalogCell courseCatelogTableViewCellWithTableView:tableView];
    
    cell.capterModel = self.myCapterMutableArray[indexPath.row];

    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 58;
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}

#pragma mark - Table view Private methods

- (void)addChapter{
    
    JZReleaseCapterViewController *releaseCapterVC = [JZReleaseCapterViewController new];
    releaseCapterVC.courseModel = self.courseModel;
    [self.navigationController pushViewController:releaseCapterVC animated:YES];

}

@end
