//
//  JZMyViewController.m
//  jz_sanrenxing
//
//  Created by GuJun on 16/9/8.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "JZMyViewController.h"
#import "JZGJLoginViewController.h"
#import "MytableViewCell.h"
#import "JZCurrentUserModel.h"
#import "UserInfoViewController.h"
#import <UIImageView+WebCache.h>
#import "JZCourseListViewController.h"

@interface JZMyViewController ()<UITableViewDelegate,UITableViewDataSource>{
    
}
/** tableview*/
@property (weak, nonatomic) IBOutlet UITableView *mytableView;

/** cell  icon  array*/
@property (nonatomic, strong) NSArray            *iconArray;

/** cell  label array*/
@property (nonatomic, strong) NSArray            *labelArray;

/** header  view*/
@property (strong, nonatomic) IBOutlet UIView    *tableHeaderView;

/** usericon*/
@property (weak, nonatomic)   IBOutlet UIImageView *headIcon;

/** username*/
@property (weak, nonatomic)   IBOutlet UILabel   *headName;


- (IBAction)LoginButton:(id)sender;

@end

@implementation JZMyViewController

- (NSArray *)iconArray{
    
    if (!_iconArray) {
        _iconArray = @[@"dingdan",@"shoucang",@"guanzhu",@"xuexijilu"];
    }
    return _iconArray;
    
}


- (NSArray *)labelArray {
    
    if (!_labelArray) {
        _labelArray = @[@"我的课程",@"我的收藏",@"发布课程",@"学习记录"];
    }
    return _labelArray;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"我的";
        
    self.mytableView.tableHeaderView = self.tableHeaderView;
    self.mytableView.separatorStyle = UITableViewCellSelectionStyleNone;
    
    self.headIcon.layer.cornerRadius = 85/2;
    self.headIcon.layer.masksToBounds = YES;

    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    JZCurrentUserModel *user = [JZCurrentUserModel sharedJZCurrentUserModel];
    if ([user isLogin]) {
        if (user.userNickname && ![user.userNickname isEqualToString:@""]) {
            _headName.text = user.userNickname;
        } else {
            _headName.text = user.phone;
        }

        [_headIcon sd_setImageWithURL:[NSURL URLWithString:user.userAvatar] placeholderImage:[UIImage imageNamed:@"login_img"]];
    } else {
        _headName.text = @"未登录";
        _headIcon.image = [UIImage imageNamed:@"login_img"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        return 4;
    }else{
        return 1;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MytableViewCell *cell = [MytableViewCell myTableViewCellWithTableView:tableView];
    
    if (indexPath.section == 1) {
        
        cell.cell_label.text = @"设置";
        cell.cell_icon.image = [UIImage imageNamed:@"shezhi"];
        
    }else{
        
        cell.cell_label.text = self.labelArray[indexPath.row];
        cell.cell_icon.image = [UIImage imageNamed:self.iconArray[indexPath.row]];

    }
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 45;
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.section) {
        case 0:
            
            if (indexPath.row == 2) {
                
                JZCourseListViewController *courseListVC = [JZCourseListViewController new];
                
                [self.navigationController pushViewController:courseListVC animated:YES];
            }
            
            break;
        default:
            
            
            break;
    }
    
}

//section底部间距
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
//section底部视图
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{

    UIView *view=[[UIView alloc] init];

    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, 10);
    view.backgroundColor = RGBCOLOR(240, 240, 240);

    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view=[[UIView alloc] init];
    
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, 10);
    view.backgroundColor = RGBCOLOR(240, 240, 240);
    
    return view;

}

//登录
- (IBAction)LoginButton:(id)sender {
    JZCurrentUserModel *user = [JZCurrentUserModel sharedJZCurrentUserModel];
    UIViewController *controller;
    if ([user isLogin]) {
        controller = [[UserInfoViewController alloc] init];
    } else {
        controller = [JZGJLoginViewController new];
    }
    [self.navigationController pushViewController:controller animated:YES];
}

@end
