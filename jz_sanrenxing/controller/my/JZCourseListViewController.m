//
//  JZCourseListViewController.m
//  jz_sanrenxing
//
//  Created by GuJun on 16/10/8.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "JZCourseListViewController.h"
#import "JZReleaseCourseViewController.h"
#import "JZCourseListTableViewCell.h"
#import "JZCourseCatalogViewController.h"
#import "JZCourseModel.h"
#import "JZGJLoginViewController.h"

@interface JZCourseListViewController ()<UITableViewDelegate,UITableViewDataSource>

/** tableview */
@property (nonatomic, strong) UITableView   *tableview;

/** 页数 */
@property (nonatomic, assign) int page;

/** 我的课程数据数组 */
@property (nonatomic, strong) NSMutableArray   *myCourseMutableArray;

@end

@implementation JZCourseListViewController

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    //刷新
    [self RefreshData];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"我的发布";
    
    [self navRightButton];
    
    [self drawTableview];
}

- (void)navRightButton{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发布课程" style:UIBarButtonItemStylePlain target:self action:@selector(releaseCourse)];
}

- (void)drawTableview{
    
    self.tableview = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    
    self.tableview.delegate = self;
    
    self.tableview.dataSource = self;
    
    [self.view addSubview:_tableview];
    
    self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark - 下拉刷新 上拉加载
- (void)RefreshData
{
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
    self.tableview.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    
    // 进入刷新状态
    [self.tableview.mj_header beginRefreshing];
    
    
    self.tableview.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];

    
}
//下拉刷新
- (void)loadNewData{
    
    //结束上拉刷新
    [self.tableview.mj_footer endRefreshing];
    
    //发送请求
    __weak typeof(self) weakSelf=self;

    [HttpUtils requestMyCourseWithPage:1 pagesize:10 callback:^(int code, NSDictionary *dict, NSString *msg) {
        
        if (1 == code) {
            // 字典 -> 模型
            weakSelf.myCourseMutableArray = [JZCourseModel mj_objectArrayWithKeyValuesArray:dict[@"list"]];
                        
            //刷新tableView
            [weakSelf.tableview reloadData];
            
            // 结束刷新
            [weakSelf.tableview.mj_header endRefreshing];
            
        } else {
            [SVProgressHUD showInfoWithStatus:msg];
            [weakSelf.tableview.mj_header endRefreshing];
        }

    }];
    
}

//上拉加载
- (void)loadMoreData{
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.myCourseMutableArray.count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    JZCourseListTableViewCell *cell = [JZCourseListTableViewCell courseListTableViewCellWithTableView:tableView];
    
    cell.courseModel = self.myCourseMutableArray[indexPath.section];
    
    
    cell.addButton.tag = indexPath.section;
    [cell.addButton addTarget:self action:@selector(AddSection:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.modifButton.tag = indexPath.section;
    [cell.modifButton addTarget:self action:@selector(modifSection:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.deleteButton.tag = indexPath.section;
    [cell.deleteButton addTarget:self action:@selector(deleteSection:) forControlEvents:UIControlEventTouchUpInside];

    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 141;
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}

//section底部间距
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
//section底部视图
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    UIView *view=[[UIView alloc] init];
    
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, 10);
    view.backgroundColor = RGBCOLOR(240, 240, 240);
    
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view=[[UIView alloc] init];
    
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, 10);
    view.backgroundColor = RGBCOLOR(240, 240, 240);
    
    return view;
    
}

#pragma mark - Private methods

- (void)AddSection:(UIButton *)sender{
    
    JZCourseCatalogViewController *courseCatalogVC = [JZCourseCatalogViewController new];
    
    courseCatalogVC.courseModel = self.myCourseMutableArray[sender.tag];
    
    [self.navigationController pushViewController:courseCatalogVC animated:YES];
    
}

- (void)releaseCourse{
    
    JZReleaseCourseViewController *releaseCourseVC = [JZReleaseCourseViewController new];
    
    releaseCourseVC.butTyle = @"确认发布";
    
    [self.navigationController pushViewController:releaseCourseVC animated:YES];
    
}


- (void)modifSection:(UIButton *)sender{
    
    JZReleaseCourseViewController *releaseCourseVC = [JZReleaseCourseViewController new];
    
    releaseCourseVC.butTyle = @"修改发布";
    
    [self.navigationController pushViewController:releaseCourseVC animated:YES];

}


- (void)deleteSection:(UIButton *)sender{
    JZCourseModel *courseModel = self.myCourseMutableArray[sender.tag];
    [HttpUtils requestDeleteCoureseWithCourseID:courseModel.course_id callback:^(int code, NSDictionary *dict, NSString *msg) {
        if (1 == code) {
            //刷新tableView
            [self.tableview reloadData];
            [SVProgressHUD showInfoWithStatus:msg];
            
        } else {
            [SVProgressHUD showInfoWithStatus:msg];
        }
        
        if ([msg isEqualToString:@"缺少token"]) {
            [self goToLogin];
        }
    }];
    
}

#pragma mark 未登录
- (void)goToLogin{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请登录" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }];
    
    UIAlertAction *ensureAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        [self.navigationController pushViewController:[JZGJLoginViewController new] animated:YES];
        
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:ensureAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}


@end
