//
//  JZSetpasswordViewController.m
//  jz_sanrenxing
//
//  Created by GuJun on 16/9/29.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "JZSetpasswordViewController.h"

@interface JZSetpasswordViewController ()

@property (weak, nonatomic) IBOutlet UITextField *password1;

@property (weak, nonatomic) IBOutlet UITextField *password2;

@property (weak, nonatomic) IBOutlet UIButton *completeButton;

- (IBAction)complete:(UIButton *)sender;


@end

@implementation JZSetpasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"设置密码";
    
    self.completeButton.layer.cornerRadius = 8;
    
    self.completeButton.layer.masksToBounds = YES;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)complete:(UIButton *)sender {

    
    
    [self.navigationController popToRootViewControllerAnimated:YES];

}
@end
