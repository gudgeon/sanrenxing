//
//  JZGJLoginViewController.m
//  jz_sanrenxing
//
//  Created by GuJun on 16/9/29.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "JZGJLoginViewController.h"
#import "JZGJRegisteredViewController.h"
#import "JZForgotPasswordViewController.h"
#import "JZCurrentUserModel.h"

@interface JZGJLoginViewController ()<UITextFieldDelegate>
{
    NSString *_phone;
    NSString *_password;
}

@property (weak, nonatomic) IBOutlet UIImageView *LoginImage;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextFiled;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextFiled;

@property (weak, nonatomic) IBOutlet UIButton *LoginButton;
@property (weak, nonatomic) IBOutlet UIButton *wechatLoginButton;
@property (weak, nonatomic) IBOutlet UIButton *qqLoginButton;
@property (weak, nonatomic) IBOutlet UIButton *sinaLoginButton;


- (IBAction)loginAction:(UIButton *)sender;
- (IBAction)registerAction:(UIButton *)sender;
- (IBAction)foundPasswordAction:(UIButton *)sender;

@end

@implementation JZGJLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self initUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initUI {
    self.navigationItem.title = @"登录";
    self.navigationController.navigationBarHidden = NO;

    _LoginImage.layer.cornerRadius = 80/2;
    _LoginImage.layer.masksToBounds = YES;

    _LoginButton.layer.cornerRadius = 4;
    _LoginButton.layer.masksToBounds = YES;

    _wechatLoginButton.layer.cornerRadius = 25;
    _wechatLoginButton.layer.masksToBounds = YES;

    _qqLoginButton.layer.cornerRadius = 25;
    _qqLoginButton.layer.masksToBounds = YES;

    _sinaLoginButton.layer.cornerRadius = 25;
    _sinaLoginButton.layer.masksToBounds = YES;
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self hideKeyboard];
}

- (void)hideKeyboard {
    [_phoneTextFiled resignFirstResponder];
    [_passwordTextFiled resignFirstResponder];
}

/**
 校验手机号

 @return 返回错误信息，输入有效时返回nil
 */
- (NSString *)checkPhone {
    _phone = _phoneTextFiled.text;
    _phone = [_phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (_phone.length < 11) {
        return @"请输入11位有效手机号码";
    }

    return nil;
}

/**
 校验密码长度

 @return 返回错误信息，错误信息为nil时表示两次输入一致
 */
- (NSString *)checkPassword {
    _password = _passwordTextFiled.text;
    _password = [_password stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (_password.length >= 6) {
        return nil;
    }

    return @"有效密码长度不能小于6位";
}

/**
 校验注册信息输入

 @return YES / NO
 */
- (BOOL)isValidRegisterInfo {

    NSString *phoneError = [self checkPhone];
    if (phoneError) {
        [SVProgressHUD showInfoWithStatus:phoneError];
        return NO;
    }

    NSString *passwordError = [self checkPassword];
    if (passwordError) {
        [SVProgressHUD showInfoWithStatus:passwordError];
        return NO;
    }

    return YES;
}

#pragma mark - Action
- (IBAction)loginAction:(UIButton *)sender {
    if (![self isValidRegisterInfo]) {
        return;
    }

    [self hideKeyboard];
    [SVProgressHUD show];
    [HttpUtils requestToLoginWithPhone:_phone password:_password callback:^(int code, NSDictionary *dict, NSString *msg) {
        if (1 == code) {
            [SVProgressHUD showSuccessWithStatus:msg];
            [[JZCurrentUserModel sharedJZCurrentUserModel] saveUserInfoWithDictionary:dict];
            [self.navigationController popToRootViewControllerAnimated:YES];
        } else {
            [SVProgressHUD showInfoWithStatus:msg];
        }
    }];

}

- (IBAction)registerAction:(UIButton *)sender {
    JZGJRegisteredViewController *registeredVC = [JZGJRegisteredViewController new];
    [self.navigationController pushViewController:registeredVC animated:YES];
}

- (IBAction)foundPasswordAction:(UIButton *)sender {
    JZForgotPasswordViewController *forgotPasswordVC = [JZForgotPasswordViewController new];
    [self.navigationController pushViewController:forgotPasswordVC animated:YES];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self hideKeyboard];
    [self loginAction:nil];
    return YES;
}


@end
