//
//  JZGJRegisteredViewController.m
//  jz_sanrenxing
//
//  Created by GuJun on 16/9/29.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "JZGJRegisteredViewController.h"
#import "JZCurrentUserModel.h"

@interface JZGJRegisteredViewController ()<UITextFieldDelegate>
{
    NSString *_phone;
    NSString *_password;
    NSString *_vCode;
}
@property (weak, nonatomic) IBOutlet UIButton *vCodeButton;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;

@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *vCodeTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPssswordTextField;

- (IBAction)vCodeAction:(UIButton *)sender;
- (IBAction)registerAction:(UIButton *)sender;
@end

@implementation JZGJRegisteredViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self initUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [_phoneTextField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self hideKeyboard];
}

- (void)initUI {
    self.navigationItem.title = @"注册";

    _vCodeButton.layer.cornerRadius = 4;
    _vCodeButton.layer.masksToBounds = YES;

    _registerButton.layer.cornerRadius = 4;
    _registerButton.layer.masksToBounds = YES;
}

- (void)hideKeyboard {
    [_phoneTextField resignFirstResponder];
    [_vCodeTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    [_confirmPssswordTextField resignFirstResponder];
}


/**
 校验密码长度

 @return 返回错误信息，错误信息为nil时表示两次输入一致
 */
- (NSString *)checkPassword {
    _password = _passwordTextField.text;
    _password = [_password stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (_password.length >= 6) {
        return nil;
    }

    return @"有效密码长度不能小于6位";
}

/**
 两次密码一致性校验

 @return 返回错误信息，错误信息为nil时表示两次输入一致
 */
- (NSString *)confirmPassword {
    NSString *password = _passwordTextField.text;
    NSString *confirm = _confirmPssswordTextField.text;
    if ([password isEqualToString: confirm]) {
        return nil;
    }
    return @"两次密码输入不一致";
}


/**
 校验注册信息输入

 @return YES / NO
 */
- (BOOL)isValidRegisterInfo {
    _vCode = _vCodeTextField.text;
    _vCode = [_vCode stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (_vCode.length != 6) {
        [SVProgressHUD showInfoWithStatus:@"验证码输入有误"];
        return NO;
    }

    NSString *passwordError = [self checkPassword];
    if (passwordError) {
        [SVProgressHUD showInfoWithStatus:passwordError];
        return NO;
    }

    NSString *confirmError = [self confirmPassword];
    if (confirmError) {
        [SVProgressHUD showInfoWithStatus:confirmError];
        return NO;
    }

    return YES;
}

#pragma mark - Action
- (IBAction)vCodeAction:(id)sender {
    _phone = _phoneTextField.text;
    _phone = [_phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (_phone.length < 11) {
        [SVProgressHUD showInfoWithStatus:@"请输入11位有效手机号码"];
        return;
    }

    [self hideKeyboard];
    [SVProgressHUD show];
    [HttpUtils requestVerificationCodeWithPhone:_phone callback:^(int code, NSString *msg) {
        if (1 == code) {
            [SVProgressHUD showSuccessWithStatus:msg];
            [_vCodeTextField becomeFirstResponder];
        } else {
            [SVProgressHUD showInfoWithStatus:msg];
        }
    }];
}

- (IBAction)registerAction:(UIButton *)sender {
    if (![self isValidRegisterInfo]) {
        return;
    }

    [self hideKeyboard];
    [SVProgressHUD show];

    [HttpUtils requestToRegisterWithPhone:_phone password:_password vCode:_vCode callback:^(int code, NSDictionary *dict, NSString *msg) {
        if (1 == code) {
            [SVProgressHUD showSuccessWithStatus:msg];
            // 保存用户信息
            [[JZCurrentUserModel sharedJZCurrentUserModel] saveUserInfoWithDictionary:dict];
            [self.navigationController popToRootViewControllerAnimated:YES];
        } else {
            [SVProgressHUD showInfoWithStatus:msg];
        }
    }];

}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self hideKeyboard];
    if (textField.returnKeyType == UIReturnKeyNext) {
        [_confirmPssswordTextField becomeFirstResponder];
    } else if (textField.returnKeyType == UIReturnKeyDone) {
        [self registerAction:nil];
    }

    return YES;
}

@end
