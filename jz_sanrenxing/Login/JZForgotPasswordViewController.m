//
//  JZForgotPasswordViewController.m
//  jz_sanrenxing
//
//  Created by GuJun on 16/9/30.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "JZForgotPasswordViewController.h"
#import "JZForgotPasswordVerificationViewController.h"

@interface JZForgotPasswordViewController ()

@property (weak, nonatomic) IBOutlet UITextField *telephone;

- (IBAction)ClicknextButton:(id)sender;

@end

@implementation JZForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.title = @"手机号";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)ClicknextButton:(id)sender {
    
    JZForgotPasswordVerificationViewController *forgotPassWordVC = [JZForgotPasswordVerificationViewController new];
    
    [self.navigationController pushViewController:forgotPassWordVC animated:YES];
}
@end
