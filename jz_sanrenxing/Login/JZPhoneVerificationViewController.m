//
//  JZPhoneVerificationViewController.m
//  jz_sanrenxing
//
//  Created by GuJun on 16/9/29.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "JZPhoneVerificationViewController.h"
#import "JZSetpasswordViewController.h"

@interface JZPhoneVerificationViewController ()

@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UITextField *inputVerificationCode;

- (IBAction)VerificationCode:(id)sender;

- (IBAction)ClickNextButton:(id)sender;

@end

@implementation JZPhoneVerificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.title = @"手机验证";
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)VerificationCode:(id)sender {
}

- (IBAction)ClickNextButton:(id)sender {
    
    JZSetpasswordViewController *setpasswordVC = [JZSetpasswordViewController new];
    
    [self.navigationController pushViewController:setpasswordVC animated:YES];
}
@end
