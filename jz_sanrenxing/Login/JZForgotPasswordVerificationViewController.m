//
//  JZForgotPasswordVerificationViewController.m
//  jz_sanrenxing
//
//  Created by GuJun on 16/9/30.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "JZForgotPasswordVerificationViewController.h"
#import "JZSetpasswordViewController.h"

@interface JZForgotPasswordVerificationViewController ()

/**验证手机号*/
@property (weak, nonatomic) IBOutlet UILabel *validationPhone;

/**验证码*/
@property (weak, nonatomic) IBOutlet UILabel *validation;

- (IBAction)ClickNextButton:(id)sender;

@end

@implementation JZForgotPasswordVerificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.title = @"验证码";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)ClickNextButton:(id)sender {
    
    JZSetpasswordViewController *setpasswordVC = [[JZSetpasswordViewController alloc] init];
    
    [self.navigationController pushViewController:setpasswordVC animated:YES];
}
@end
