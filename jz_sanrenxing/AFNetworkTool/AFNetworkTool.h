//
//  AFNetworkTool.h
//  JYSmartDemo
//
//  Created by Killua on 16/4/2.
//  Copyright © 2016年 JingZhong. All rights reserved.
//

/**
 1. 需要注意的是,默认提交请求的数据是二进制的,返回格式是JSON
 
 1> 如果提交数据是JSON的,需要将请求格式设置为AFJSONRequestSerializer
 2> 如果返回格式不是JSON的,
 
 2. 请求格式
 
 AFHTTPRequestSerializer            二进制格式
 AFJSONRequestSerializer            JSON
 AFPropertyListRequestSerializer    PList(是一种特殊的XML,解析起来相对容易)
 
 3. 返回格式
 
 AFHTTPResponseSerializer           二进制格式
 AFJSONResponseSerializer           JSON
 AFXMLParserResponseSerializer      XML,只能返回XMLParser,还需要自己通过代理方法解析
 AFXMLDocumentResponseSerializer (Mac OS X)
 AFPropertyListResponseSerializer   PList
 AFImageResponseSerializer          Image
 AFCompoundResponseSerializer       组合
 */

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworkReachabilityManager.h>

//@class PodsDummy_AFNetworking;
@interface AFNetworkTool : NSObject

/**
 *  检测网络状态
 *
 *  @return AFNetworkReachabilityStatus
 */
+ (AFNetworkReachabilityStatus)netWorkStatus;

/**
 *  JSON方式获取数据
 *
 *  @param url     服务器地址
 *  @param success 成功回调
 *  @param fail    失败回调
 */
+ (void)JSONDataWithUrl:(NSString *)urlStr success:(void (^)(id json))success fail:(void (^)())fail;

/**
 *  XML方式获取数据
 *
 *  @param urlStr  服务器地址
 *  @param success 成功回调
 *  @param fail    失败回调
 */
+ (void)XMLDataWithUrl:(NSString *)urlStr success:(void (^)(id xml))success fail:(void (^)())fail;

/**
 *  Session下载文件
 *
 *  @param urlStr  服务器地址
 *  @param success 成功回调
 *  @param fail    失败回调
 */
+ (void)sessionDownloadWithUrl:(NSString *)urlStr success:(void (^)(NSURL *fileURL))success fail:(void (^)())fail;

/**
 *  JSON方式GET提交数据
 *
 *  @param urlStr     服务器地址
 *  @param parameters 参数
 *  @param success    成功回调
 *  @param fail       失败回调
 */
+ (void)getJSONWithUrl:(NSString *)urlStr parameters:(id)parameters success:(void (^)(id responseObject))success fail:(void (^)())fail;

/**
 *  GET提交数据
 *
 *  @param urlStr     服务器地址
 *  @param parameters 服务器地址
 *  @param success    成功回调
 *  @param fail       失败回调
 */
+ (void)getWithUrl:(NSString *)urlStr parameters:(id)parameters success:(void (^)(id responseObject))success fail:(void (^)())fail;

/**
 *  JSON方式POST提交数据
 *
 *  @param urlStr     服务器地址
 *  @param parameters 参数
 *  @param success    成功回调
 *  @param fail       失败回调
 */
+ (void)postJSONWithUrl:(NSString *)urlStr parameters:(id)parameters success:(void (^)(id responseObject))success fail:(void (^)())fail;

/**
 *  POST提交数据
 *
 *  @param urlStr     服务器地址
 *  @param parameters 参数
 *  @param success    成功回调
 *  @param fail       失败回调
 */
+ (void)postWithUrl:(NSString *)urlStr parameters:(id)parameters success:(void (^)(id responseObject))success fail:(void (^)())fail;

/**
 *  文件上传
 *
 *  @param urlStr   服务器地址
 *  @param fileURL  文件路径
 *  @param fileName 保存的文件名
 *  @param fileTye  文件类型
 *  @param success  成功回调
 *  @param fail     失败回调
 */
+ (void)postUploadWithUrl:(NSString *)urlStr fileUrl:(NSURL *)fileURL fileName:(NSString *)fileName fileType:(NSString *)fileTye success:(void (^)(id responseObject))success fail:(void (^)())fail;

/**
 *  JSON方式POST提交数据 带文件
 *
 *  @param urlStr     服务器地址
 *  @param parameters json参数
 *  @param urls       文件数组
 *  @param fileKey    key
 *  @param fileType   文件类型
 *  @param success    成功回调
 *  @param fail       失败回调
 */
+ (void)postJSONWithUrl:(NSString *)urlStr parameters:(id)parameters fileUrls:(NSArray *)urls fileKey:(NSString *)fileKey fileType:(NSString *)fileType success:(void (^)(id responseObject))success fail:(void (^)())fail;



/**
 提交数据 带文件

 @param urlStr     服务器地址
 @param parameters 参数
 @param fileURLs   文件数组
 @param fileKey    key
 @param fileType   文件类型
 @param success    成功回调
 @param fail       失败回调
 */
+ (void)postWithUrl:(NSString *)urlStr parameters:(id)parameters fileURLs:(NSArray *)fileURLs fileKey:(NSString *)fileKey fileType:(NSString *)fileType success:(void (^)(id responseObject))success fail:(void (^)())fail;


@end
