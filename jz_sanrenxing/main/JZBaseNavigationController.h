//
//  JZBaseNavigationController.h
//  jz_easyAndEasyToTravel
//
//  Created by Wolfe on 16/6/21.
//  Copyright © 2016年 杭州景忠科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JZBaseNavigationController : UINavigationController

@end
