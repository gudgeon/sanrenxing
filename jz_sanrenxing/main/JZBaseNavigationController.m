//
//  JZBaseNavigationController.m
//  jz_easyAndEasyToTravel
//
//  Created by Wolfe on 16/6/21.
//  Copyright © 2016年 杭州景忠科技有限公司. All rights reserved.
//

#import "JZBaseNavigationController.h"

@interface JZBaseNavigationController () <UIGestureRecognizerDelegate>

@end

@implementation JZBaseNavigationController

/**
 * 当第一次使用这个类的时候会调用一次
 */
+ (void)initialize
{
    // 当导航栏用在XMGNavigationController中, appearance设置才会生效
    //    UINavigationBar *bar = [UINavigationBar appearanceWhenContainedIn:[self class], nil];
    UINavigationBar *bar = [UINavigationBar appearance];

    bar.translucent = NO;
    
    
    //设置背景色
    [bar setBarTintColor:[UIColor colorWithRed:242/255.f green:244/255.f blue:245/255.f alpha:0]];
    //设置有导航条时，状态栏的文字颜色
    [bar setBarStyle:UIBarStyleBlack];
    
    //设置导航条的背景图
    //[[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"NavigationBarDefault"] forBarMetrics:UIBarMetricsDefault];
    //设置左右按钮上的文字颜色
    [bar setTintColor:[UIColor blackColor]];
    //设置返回按钮中出现的箭头样式
    //[[UINavigationBar appearance] setBackIndicatorImage:[UIImage imageNamed:@"back_btn"]];
    //[[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"back_btn"]];
    //修改中间题目的文字样式，添加阴影，修改字体大小
    //    NSShadow *shadow = [NSShadow new];
    //    shadow.shadowColor = [UIColor redColor];
    //    shadow.shadowOffset = CGSizeMake(0, 1);
    //题目展示的相关属性,其中键只能死记硬背
    //NSDictionary *attrDic = @{NSShadowAttributeName:shadow, NSFontAttributeName:[UIFont systemFontOfSize:21], NSForegroundColorAttributeName:[UIColor greenColor]};
    NSDictionary *attrDic = @{NSFontAttributeName:[UIFont systemFontOfSize:16], NSForegroundColorAttributeName:[UIColor colorWithRed:43/255.f green:44/255.f blue:45/255.f alpha:1]};
    [bar setTitleTextAttributes:attrDic];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

/**
 * 可以在这个方法中拦截所有push进来的控制器
 */
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (self.childViewControllers.count > 0) { // 如果push进来的不是第一个控制器
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        //[button setTitle:@"返回" forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
        //[button setImage:[UIImage imageNamed:@"navigationButtonReturnClick"] forState:UIControlStateHighlighted];
        [button setFrame:CGRectMake(0, 0, 24, 24)];
        // 让按钮内部的所有内容左对齐
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        
        [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        
        // 修改导航栏左边的item
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        // 隐藏tabbar
        viewController.hidesBottomBarWhenPushed = YES;
    }
    
    // 这句super的push要放在后面, 让viewController可以覆盖上面设置的leftBarButtonItem
    [super pushViewController:viewController animated:animated];
    
}

- (void)back {
    
    [self popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
