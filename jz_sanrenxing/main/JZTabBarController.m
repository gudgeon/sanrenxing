//
//  JZTabBarController.m
//  jz_easyAndEasyToTravel
//
//  Created by Wolfe on 16/6/21.
//  Copyright © 2016年 杭州景忠科技有限公司. All rights reserved.
//

#import "JZTabBarController.h"
#import "JZBaseNavigationController.h"
#import "JZMyViewController.h"
#import "JZHomePageViewController.h"
#import "JZTeacherViewController.h"


@interface JZTabBarController()

@end

@implementation JZTabBarController

+ (void)initialize
{
    // 通过appearance统一设置所有UITabBarItem的文字属性
    // 后面带有UI_APPEARANCE_SELECTOR的方法, 都可以通过appearance对象来统一设置
    NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
    attrs[NSFontAttributeName] = [UIFont systemFontOfSize:13];
    attrs[NSForegroundColorAttributeName] = [UIColor blackColor];
    
    NSMutableDictionary *selectedAttrs = [NSMutableDictionary dictionary];
    selectedAttrs[NSFontAttributeName] = attrs[NSFontAttributeName];
    selectedAttrs[NSForegroundColorAttributeName] = [UIColor colorWithRed:7/255.f green:48/255.f blue:156/255.f alpha:1.0];
    
    UITabBarItem *item = [UITabBarItem appearance];
    [item setTitleTextAttributes:attrs forState:UIControlStateNormal];
    [item setTitleTextAttributes:selectedAttrs forState:UIControlStateSelected];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // 添加子控制器
    [self setupChildVc:[[JZHomePageViewController alloc] init] title:@"首页" image:@"shouye_n" selectedImage:@"shouyetab"];
    
    [self setupChildVc:[[JZTeacherViewController alloc] init] title:@"老师" image:@"xingcheng_n" selectedImage:@"xingcheng"];
    
    [self setupChildVc:[[JZMyViewController alloc] init] title:@"我的" image:@"wode_n" selectedImage:@"wode"];
}

/**
 * 初始化子控制器
 */
- (void)setupChildVc:(UIViewController *)vc title:(NSString *)title image:(NSString *)image selectedImage:(NSString *)selectedImage{
    // 设置文字和图片
    vc.navigationItem.title = title;
    vc.tabBarItem.title = title;
    vc.tabBarItem.image = [UIImage imageNamed:image];
    vc.tabBarItem.selectedImage = [UIImage imageNamed:selectedImage];
    
    // 包装一个导航控制器, 添加导航控制器为tabbarcontroller的子控制器
    JZBaseNavigationController *nav = [[JZBaseNavigationController alloc] initWithRootViewController:vc];
    [self addChildViewController:nav];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
