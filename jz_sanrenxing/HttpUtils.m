//
//  HttpUtils.m
//  jz_sanrenxing
//
//  Created by Killua on 2016/10/5.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "HttpUtils.h"
#import "AFNetworkTool.h"
#import "JZCurrentUserModel.h"

@implementation HttpUtils

+ (void)requestVerificationCodeWithPhone:(NSString *)phone callback:(CallbackNoData)callback {
    NSString *url = [NSString stringWithFormat:@"%@%@", HOST, verificationCode];
    NSDictionary *dict = @{@"user.user_phone": phone};
    [AFNetworkTool postWithUrl:url parameters:dict success:^(id responseObject) {
        if (callback) {
            int code = [responseObject[@"code"] intValue];
            NSString *msg = responseObject[@"message"];
            callback(code, msg);
        }
    } fail:^{
        if (callback) {
            callback(SYSTEM_ERROR, @"系统错误，请求没有到服务器");
        }
    }];
}

+ (void)requestToRegisterWithPhone:(NSString *)phone password:(NSString *)password vCode:(NSString *)vCode callback:(CallbackDictionary)callback {
    NSString *url = [NSString stringWithFormat:@"%@%@", HOST, userRegister];
    NSDictionary *dict = @{@"user.user_phone": phone,
                           @"user.user_password": password,
                           @"user.captcha_code": vCode,
                           @"user.client_type": KEY_CLIENT_TYPE,
                           @"user.client_imei": UUID};

    [AFNetworkTool postWithUrl:url parameters:dict success:^(id responseObject) {
        if (callback) {
            int code = [responseObject[@"code"] intValue];
            NSString *msg = responseObject[@"message"];
            callback(code, responseObject, msg);
        }
    } fail:^{
        if (callback) {
            callback(SYSTEM_ERROR, nil, @"系统错误，请求没有到服务器");
        }
    }];
}

+ (void)requestToLoginWithPhone:(NSString *)phone password:(NSString *)password callback:(CallbackDictionary)callback {
    NSString *url = [NSString stringWithFormat:@"%@%@", HOST, userLogin];
    NSDictionary *dict = @{@"user.user_phone": phone,
                           @"user.user_password": password};

    [AFNetworkTool postWithUrl:url parameters:dict success:^(id responseObject) {
        if (callback) {
            int code = [responseObject[@"code"] intValue];
            NSString *msg = responseObject[@"message"];
            callback(code, responseObject, msg);
        }
    } fail:^{
        if (callback) {
            callback(SYSTEM_ERROR, nil, @"系统错误，请求没有到服务器");
        }
    }];
}

+ (void)requestToUpdateUserInfoWithNickName:(NSString *)nickName email:(NSString *)email phone:(NSString *)phone name:(NSString *)name fileURL:(NSURL *)fileURL gender:(NSString *)gender birthday:(NSString *)birthday brief:(NSString *)brief careers:(NSString *)careers callback:(CallbackDictionary)callback {
    NSString *url = [NSString stringWithFormat:@"%@%@", HOST, updateUserInfo];

    JZCurrentUserModel *user = [JZCurrentUserModel sharedJZCurrentUserModel];
    NSMutableDictionary *dict = [@{@"user.user_id": user.userId} mutableCopy];
    if (nickName) {
        dict[@"user.user_nickname"] = nickName;
    }

    if (email) {
        dict[@"user.user_email"] = email;
    }

    if (phone) {
        dict[@"user.user_phone"] = phone;
    }

    if (name) {
        dict[@"userDetail.user_name"] = name;
    }

    if (gender) {
        int num = 3;
        if ([gender isEqualToString:@"男"]) {
            num = 1;
        } else if ([gender isEqualToString:@"女"]) {
            num = 2;
        }
        dict[@"userDetail.user_gender"] = @(num);
    }

    if (birthday) {
        dict[@"userDetail.user_birthday"] = birthday;
    }

    if (brief) {
        dict[@"userDetail.user_brief"] = brief;
    }

    if (careers) {
        dict[@"userDetail.user_careers"] = careers;
    }

    NSArray *array;
    if (fileURL) {
        array = @[fileURL];
    }

    [AFNetworkTool postWithUrl:url parameters:dict fileURLs:array fileKey:@"avatar" fileType:@"image/png" success:^(id responseObject) {
        if (callback) {
            int code = [responseObject[@"code"] intValue];
            NSString *msg = responseObject[@"message"];
            callback(code, responseObject, msg);
        }
    } fail:^{
        if (callback) {
            callback(SYSTEM_ERROR, nil, @"系统错误，请求没有到服务器");
        }
    }];
}


+ (void)requestIndexDatacallback:(CallbackDictionary)callback{
    NSString *url = [NSString stringWithFormat:@"%@%@", HOST, CourseList];
    [AFNetworkTool postWithUrl:url parameters:nil success:^(id responseObject) {
        if (callback) {
            int code = [responseObject[@"code"] intValue];
            NSString *msg = responseObject[@"message"];
            callback(code, responseObject, msg);
        }
    } fail:^{
        if (callback) {
            callback(SYSTEM_ERROR,nil, @"系统错误，请求没有到服务器");
        }
    }];
}


+(void)requestTeacherDatacallback:(CallbackDictionary)callback{
    
    NSDictionary *dict = @{@"user.user_role":@2,@"orderBy":@1,@"page":@1,@"pagesize":@10};
    NSString *url = [NSString stringWithFormat:@"%@%@", HOST, getTeacherInfo];
    [AFNetworkTool postWithUrl:url parameters:dict success:^(id responseObject) {
        if (callback) {
            int code = [responseObject[@"code"] intValue];
            NSString *msg = responseObject[@"message"];
            callback(code, responseObject, msg);
        }
    } fail:^{
        if (callback) {
            callback(SYSTEM_ERROR,nil, @"系统错误，请求没有到服务器");
        }
    }];

}


+ (void)requestToAddCourseWithCourse_name:(NSString *)course_name
                             course_brief:(NSString *)course_brief
                           course_keyword:(NSString *)course_keyword
                              coverAvatar:(NSURL    *)coverAvatar
                          course_property:(NSString *)course_property
                               course_pay:(double    )course_pay
                          course_part_num:(int       )course_part_num
                             course_bdate:(NSString *)course_bdate
                             course_edate:(NSString *)course_edate
                              course_time:(NSString *)course_time
                                   is_hot:(NSString *)is_hot
                             is_recommend:(NSString *)is_recommend
                  crowdfunding_target_num:(int       )crowdfunding_target_num
                crowdfunding_target_money:(double    )crowdfunding_target_money
                                 callback:(CallbackDictionary)callback{
    NSString *url = [NSString stringWithFormat:@"%@%@", HOST, addCourse];

    JZCurrentUserModel *user = [JZCurrentUserModel sharedJZCurrentUserModel];
    NSMutableDictionary *dict = [@{@"course.user_id": user.userId,@"course.type_id":@"1"} mutableCopy];
    if (course_name) {
        dict[@"course.course_name"] = course_name;
    }
    
    if (course_brief) {
        dict[@"course.course_brief"] = course_brief;
    }
    
    if (course_keyword) {
        dict[@"course.course_keyword"] = course_keyword;
    }
    
    if (course_property) {
        dict[@"course.course_property"] = course_property;
    }
    
   
    dict[@"course.course_pay"] = [NSNumber numberWithDouble:course_pay];
    

    if (course_part_num) {
        dict[@"course.course_part_num"] = [NSNumber numberWithInt:course_part_num];
    }
    
    if (crowdfunding_target_num) {
        dict[@"crowdfunding.crowdfunding_target_num"] = [NSNumber numberWithDouble:crowdfunding_target_num];
    }
    
    if (crowdfunding_target_money) {
        dict[@"crowdfunding.crowdfunding_target_money"] = [NSNumber numberWithInt:crowdfunding_target_money];
    }

    if (course_bdate) {
        dict[@"course.course_bdate"] = course_bdate;
    }
    
    if (course_edate) {
        dict[@"course.course_edate"] = course_edate;
    }

    if (course_bdate) {
        dict[@"crowdfunding.crowdfunding_bdate"] = course_bdate;
    }
    
    if (course_edate) {
        dict[@"crowdfunding.crowdfunding_edate"] = course_edate;
    }
    
    if (course_time) {
        dict[@"course.course_time"] = course_time;
    }
    
    if (is_hot) {
        dict[@"course.is_hot"] = is_hot;
    }

    if (is_recommend) {
        dict[@"course.is_recommend"] = is_recommend;
    }

    
    
    NSArray *array;
    if (coverAvatar) {
        array = @[coverAvatar];
    }
    
    [AFNetworkTool postWithUrl:url parameters:dict fileURLs:array fileKey:@"coverAvatar" fileType:@"image/png" success:^(id responseObject) {
        if (callback) {
            int code = [responseObject[@"code"] intValue];
            NSString *msg = responseObject[@"message"];
            callback(code, responseObject, msg);
        }
    } fail:^{
        if (callback) {
            callback(SYSTEM_ERROR, nil, @"系统错误，请求没有到服务器");
        }
    }];
}



+(void)requestMyCourseWithPage:(int)page pagesize:(int)pagesize callback:(CallbackDictionary)callback{
    
    NSString *url = [NSString stringWithFormat:@"%@%@", HOST, getCourseList];
    
    JZCurrentUserModel *user = [JZCurrentUserModel sharedJZCurrentUserModel];
    NSMutableDictionary *dict = [@{@"course.user_id": user.userId} mutableCopy];

    if (page) {
        dict[@"page"] = [NSNumber numberWithDouble:page];
    }
    
    if (pagesize) {
        dict[@"pagesize"] = [NSNumber numberWithInt:pagesize];
    }

    [AFNetworkTool postWithUrl:url parameters:dict success:^(id responseObject) {
        if (callback) {
            int code = [responseObject[@"code"] intValue];
            NSString *msg = responseObject[@"message"];
            callback(code, responseObject, msg);
        }
    } fail:^{
        if (callback) {
            callback(SYSTEM_ERROR,nil, @"系统错误，请求没有到服务器");
        }
    }];
    
}



+(void)UploadCapterWithPart_name:(NSString *)part_name course_id:(NSString *)course_id part_url:(NSURL *)part_url part_brief:(NSString *)part_brief part_keyword:(NSString *)part_keyword callback:(CallbackDictionary)callback{
    NSString *url = [NSString stringWithFormat:@"%@%@", HOST, uploadCapter];
    
    NSDictionary *dict = @{
                           @"part.course_id":course_id,
                           @"part.part_name":part_name,
                           @"part.part_url":part_url,
                           @"part.part_brief":part_brief,
                           @"part.part_keyword":part_keyword
                           };
    
    [AFNetworkTool postWithUrl:url parameters:dict success:^(id responseObject) {
        if (callback) {
            int code = [responseObject[@"code"] intValue];
            NSString *msg = responseObject[@"message"];
            callback(code, responseObject, msg);
        }
    } fail:^{
        if (callback) {
            callback(SYSTEM_ERROR,nil, @"系统错误，请求没有到服务器");
        }
    }];

}


    
+(void)requestVideoURLWithVideoID:(NSString *)video_id client_id:(NSString *)client_id callback:(CallbackDictionary)callback{
    
    NSString *url = @"https://openapi.youku.com/v2/videos/show.json";
    
    NSDictionary *dict = @{
                           @"video_id":video_id,
                           @"client_id":client_id,
                           
                           };
    
    [AFNetworkTool postWithUrl:url parameters:dict success:^(id responseObject) {
        if (callback) {
            int code = [responseObject[@"code"] intValue];
            NSString *msg = responseObject[@"message"];
            callback(code, responseObject, msg);
        }
    } fail:^{
        if (callback) {
            callback(SYSTEM_ERROR,nil, @"系统错误，请求没有到服务器");
        }
    }];
}

+ (void)requestMyCapterListWithPage:(int)page pagesize:(int)pagesize courseID:(NSString *)course_id callback:(CallbackDictionary)callback{
    NSString *url = [NSString stringWithFormat:@"%@%@", HOST, getCapterList];
    
    
    NSDictionary *dict = @{
                           
                           @"page":[NSNumber numberWithInt:page],
                           @"pagesize":[NSNumber numberWithInt:pagesize],
                           @"part.course_id":course_id
                           
                           };
    
    [AFNetworkTool postWithUrl:url parameters:dict success:^(id responseObject) {
        if (callback) {
            int code = [responseObject[@"code"] intValue];
            NSString *msg = responseObject[@"message"];
            callback(code, responseObject, msg);
        }
    } fail:^{
        if (callback) {
            callback(SYSTEM_ERROR,nil, @"系统错误，请求没有到服务器");
        }
    }];
    
}

+(void)requestDeleteCoureseWithCourseID:(NSString *)course_id callback:(CallbackDictionary)callback{
    
    NSString *url = [NSString stringWithFormat:@"%@%@", HOST, deleteCourse];
    NSDictionary *dict = @{@"course.course_id":course_id};
    [AFNetworkTool postWithUrl:url parameters:dict success:^(id responseObject) {
        if (callback) {
            int code = [responseObject[@"code"] intValue];
            NSString *msg = responseObject[@"message"];
            callback(code,responseObject,msg);
        }
    } fail:^{
        if (callback) {
            callback(SYSTEM_ERROR,nil, @"系统错误，请求没有到服务器");
        }
    }];
}

@end
