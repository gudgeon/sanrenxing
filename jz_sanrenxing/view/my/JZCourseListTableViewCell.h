//
//  JZCourseListTableViewCell.h
//  jz_sanrenxing
//
//  Created by GuJun on 16/10/9.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JZCourseModel.h"

@interface JZCourseListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *courseImageView;

@property (weak, nonatomic) IBOutlet UILabel  *courseTitle;

@property (weak, nonatomic) IBOutlet UIButton *remainingDay;

@property (weak, nonatomic) IBOutlet UILabel  *courseIntroduction;

@property (weak, nonatomic) IBOutlet UIButton *modifButton;

@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@property (weak, nonatomic) IBOutlet UIButton *addButton;

@property (nonatomic, strong) JZCourseModel   *courseModel;

+ (instancetype)courseListTableViewCellWithTableView:(UITableView *)tableView;

@end
