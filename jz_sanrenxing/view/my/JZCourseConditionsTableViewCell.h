//
//  JZCourseConditionsTableViewCell.h
//  jz_sanrenxing
//
//  Created by GuJun on 16/10/8.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JZCourseConditionsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;

@property (weak, nonatomic) IBOutlet UITextField *textfield;

+ (instancetype)TableViewCellWithTableView:(UITableView *)tableView;


@end
