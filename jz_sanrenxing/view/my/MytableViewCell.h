//
//  MytableViewCell.h
//  jz_sanrenxing
//
//  Created by GuJun on 16/9/30.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MytableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *cell_icon;

@property (weak, nonatomic) IBOutlet UILabel *cell_label;


+ (instancetype)myTableViewCellWithTableView:(UITableView *)tableView;

@end
