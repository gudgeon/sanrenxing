//
//  JZCourseListTableViewCell.m
//  jz_sanrenxing
//
//  Created by GuJun on 16/10/9.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "JZCourseListTableViewCell.h"
#import "JZImageModel.h"
@implementation JZCourseListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (instancetype)courseListTableViewCellWithTableView:(UITableView *)tableView{
    
    static NSString *cellID = @"courseListCellID";
    JZCourseListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:NSStringFromClass([JZCourseListTableViewCell class]) owner:nil options:nil] lastObject];
        cell.remainingDay.userInteractionEnabled = NO;
        
    }
    return cell;

}

- (void)setCourseModel:(JZCourseModel *)courseModel{
    
    JZImageModel *imageModel = courseModel.imgs[0];
    [self.courseImageView sd_setImageWithURL:[NSURL URLWithString:imageModel.img_url]];
    
    self.courseTitle.text = courseModel.course_name;
    
    self.courseIntroduction.text = courseModel.course_brief;
    
    

}

@end
