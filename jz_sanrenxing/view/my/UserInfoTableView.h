//
//  UserInfoTableView.h
//  jz_sanrenxing
//
//  Created by Killua on 2016/10/6.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UserInfoTableViewDelegate <NSObject>

@optional
- (void)touchTableView;

@end

@interface UserInfoTableView : UITableView

@property (weak, nonatomic) id<UserInfoTableViewDelegate> touchDelegate;

@end
