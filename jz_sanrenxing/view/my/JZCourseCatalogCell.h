//
//  JZCourseCatalogCell.h
//  jz_sanrenxing
//
//  Created by GuJun on 16/10/9.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JZCapterModel.h"

@interface JZCourseCatalogCell : UITableViewCell

+ (instancetype)courseCatelogTableViewCellWithTableView:(UITableView *)tableView;

@property (nonatomic, strong)JZCapterModel   *capterModel;

@property (weak, nonatomic) IBOutlet UILabel *capterTitle;


@end
