//
//  InputTableViewCell.m
//  jz_sanrenxing
//
//  Created by Killua on 2016/10/6.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "InputTableViewCell.h"
#import "JZCurrentUserModel.h"

static NSString *inputCell = @"inputCell";

@interface InputTableViewCell ()

@end

@implementation InputTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    InputTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:inputCell];
    if (cell == nil) {
        cell=[[[NSBundle mainBundle]loadNibNamed:@"InputTableViewCell" owner:nil options:nil]firstObject];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.inputTextField.text = [JZCurrentUserModel sharedJZCurrentUserModel].userNickname;
    }
    return cell;
}

@end
