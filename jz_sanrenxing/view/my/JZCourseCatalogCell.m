//
//  JZCourseCatalogCell.m
//  jz_sanrenxing
//
//  Created by GuJun on 16/10/9.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "JZCourseCatalogCell.h"

@implementation JZCourseCatalogCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
+ (instancetype)courseCatelogTableViewCellWithTableView:(UITableView *)tableView{
    
    static NSString *cellID = @"courseCatelogCellID";
    JZCourseCatalogCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:NSStringFromClass([JZCourseCatalogCell class]) owner:nil options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;

    
}

- (void)setCapterModel:(JZCapterModel *)capterModel{
    self.capterTitle.text = capterModel.part_name;
}

@end
