//
//  JZCourseConditionsTableViewCell.m
//  jz_sanrenxing
//
//  Created by GuJun on 16/10/8.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "JZCourseConditionsTableViewCell.h"

@implementation JZCourseConditionsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(instancetype)TableViewCellWithTableView:(UITableView *)tableView{
    static NSString *TableViewCellID = @"Cell";
    JZCourseConditionsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellID];
    
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:NSStringFromClass([JZCourseConditionsTableViewCell class]) owner:nil options:nil] lastObject];
        
    }
    return cell;
}

@end
