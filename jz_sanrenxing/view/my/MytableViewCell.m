//
//  MytableViewCell.m
//  jz_sanrenxing
//
//  Created by GuJun on 16/9/30.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "MytableViewCell.h"

@implementation MytableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (instancetype)myTableViewCellWithTableView:(UITableView *)tableView {
    
    static NSString *TableViewCellID = @"myTableViewCell";
    MytableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellID];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:NSStringFromClass([MytableViewCell class]) owner:nil options:nil] lastObject];
        
    }
    return cell;
    
}

@end
