//
//  UserInfoTableView.m
//  jz_sanrenxing
//
//  Created by Killua on 2016/10/6.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "UserInfoTableView.h"

@implementation UserInfoTableView

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if ([self.touchDelegate respondsToSelector:@selector(touchTableView)]) {
        [self.touchDelegate touchTableView];
    }
}

@end
