//
//  JZDropDownMenu.h
//  jz_sanrenxing
//
//  Created by GuJun on 16/10/9.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JZDropDownMenu;

@protocol JZDropDownMenuDelegate <NSObject>

- (void)selectIndex:(NSInteger)index AtDMDropDownMenu:(JZDropDownMenu *)dmDropDownMenu;

@end


@interface JZDropDownMenu : UIView<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong)UILabel      *titleLabel;        //  标题
@property(nonatomic,strong)UILabel      *curText;           //  当前选中内容
@property(nonatomic,strong)UIImageView  *arrowImg;          //  箭头图片
@property(nonatomic,strong)NSString     *arrowImageName;
@property(nonatomic,strong)UIButton     *menuBtn;
@property(nonatomic,strong)UITableView  *menuTableView;
@property(nonatomic,strong)NSArray      *listArr;
@property(nonatomic,assign)BOOL         isOpen;
@property(nonatomic,assign)id<JZDropDownMenuDelegate>delegate;

- (void)p_setUpView;
- (void)setListArray:(NSArray *)arr;
- (void)tapAction;
- (void)setTitle:(NSString *)str;
- (void)setTitleHeight:(CGFloat)height;
- (void)awakeFromNib;


@end
