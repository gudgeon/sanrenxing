//
//  JZHomePageHeadView.m
//  jz_sanrenxing
//
//  Created by GuJun on 16/10/5.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "JZHomePageHeadView.h"

@implementation JZHomePageHeadView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        //title
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.font = [UIFont systemFontOfSize:15];
        [self addSubview:self.titleLabel];
        
        // 更多
        self.moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.moreButton setTitle:@"更多" forState:UIControlStateNormal];
        [self.moreButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.moreButton setImage:[UIImage imageNamed:@"gengduo_index"] forState:UIControlStateNormal];
        self.moreButton.titleLabel.font = [UIFont systemFontOfSize:13];
        self.moreButton.titleEdgeInsets = UIEdgeInsetsMake(0, -30, 0, 0);
        self.moreButton.imageEdgeInsets = UIEdgeInsetsMake(0, 30, 0, 0);
        [self addSubview:self.moreButton];
        
        // 添加约束
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).with.offset(12);
            make.bottom.mas_equalTo(self).with.offset(-10);
        }];
        
        [self.moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).with.offset(-12);
            make.bottom.mas_equalTo(self).with.offset(-10);
        }];
        
        
    }
    
    return self;
}


@end
