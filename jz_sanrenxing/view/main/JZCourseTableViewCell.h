//
//  JZCourseTableViewCell.h
//  jz_sanrenxing
//
//  Created by GuJun on 16/9/30.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JZCourseModel.h"

@interface JZCourseTableViewCell : UITableViewCell

+ (instancetype)mainTableViewCellWithTableView:(UITableView *)tableView;

@property (nonatomic,strong) JZCourseModel   *courseModel;

@end
