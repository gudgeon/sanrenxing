//
//  JZHomePageHeadView.h
//  jz_sanrenxing
//
//  Created by GuJun on 16/10/5.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JZHomePageHeadView : UIView

//分区标题
@property (nonatomic, strong) UILabel   *titleLabel;

//更多
@property (nonatomic, strong) UIButton  *moreButton;


@end
