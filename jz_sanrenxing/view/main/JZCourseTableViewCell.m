//
//  JZCourseTableViewCell.m
//  jz_sanrenxing
//
//  Created by GuJun on 16/9/30.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "JZCourseTableViewCell.h"
#import "JZImageModel.h"

@interface JZCourseTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *courseImg;

@property (weak, nonatomic) IBOutlet UILabel *courseTitle;

@property (weak, nonatomic) IBOutlet UILabel *courseComeform;

@property (weak, nonatomic) IBOutlet UILabel *num;

@property (weak, nonatomic) IBOutlet UILabel *courseStartNum;

@property (weak, nonatomic) IBOutlet UILabel *courseStartAmont;

@property (weak, nonatomic) IBOutlet UILabel *courseAlreadyPeopleNum;

@property (weak, nonatomic) IBOutlet UILabel *courseAlreadyPeopleAmont;

@property (weak, nonatomic) IBOutlet UILabel *courseRatio;

@property (weak, nonatomic) IBOutlet UIImageView *courseIsCollection;

@end

@implementation JZCourseTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (instancetype)mainTableViewCellWithTableView:(UITableView *)tableView{
    
    static NSString *courseTableViewCellID = @"course_CellID";
    JZCourseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:courseTableViewCellID];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:NSStringFromClass([JZCourseTableViewCell class]) owner:nil options:nil] lastObject];
        
    }
    return cell;

    
}

-(void)setCourseModel:(JZCourseModel *)courseModel{
    
    JZImageModel *imageModel = courseModel.imgs[0];
    [self.courseImg sd_setImageWithURL:[NSURL URLWithString:imageModel.img_url]];
    
    self.courseTitle.text = courseModel.course_name;
    
    self.courseComeform.text = courseModel.course_keyword;
    
    self.num.text = [NSString stringWithFormat:@"%d", courseModel.course_study_num];
    
}

@end
