//
//  JZTeacherCollectionReusableView.m
//  jz_sanrenxing
//
//  Created by GuJun on 16/9/30.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "JZTeacherCollectionReusableView.h"

@implementation JZTeacherCollectionReusableView


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor blueColor];
        // 更多
        UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        moreButton.backgroundColor = [UIColor yellowColor];
        [moreButton setTitle:@"更多" forState:UIControlStateNormal];
        [moreButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [moreButton setImage:[UIImage imageNamed:@"gengduo"] forState:UIControlStateNormal];
        moreButton.titleLabel.font = [UIFont systemFontOfSize:13];
        moreButton.titleEdgeInsets = UIEdgeInsetsMake(0, -35, 0, 0);
        moreButton.imageEdgeInsets = UIEdgeInsetsMake(0, 30, 0, -5);
        [self addSubview:moreButton];

        // 添加约束
        [moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).with.offset(-12);
            make.bottom.mas_equalTo(self).with.offset(-10);
        }];
    }
    
    return self;
}

@end
