//
//  TeacherCollectionViewCell.h
//  jz_sanrenxing
//
//  Created by GuJun on 16/9/30.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JZRaiseModel.h"
#import "JZRaiseUserDetailModel.h"

@interface TeacherCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *teacherIcon;

@property (weak, nonatomic) IBOutlet UILabel     *teacherName;

@property (weak, nonatomic) IBOutlet UIButton    *isCollection;

@property (weak, nonatomic) IBOutlet UILabel     *Introduction;

@property (nonatomic, strong) JZRaiseModel       *Raisemodel;

@property (nonatomic, strong) JZRaiseUserDetailModel       *raiseUserDetailModel;

@end
