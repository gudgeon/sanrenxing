//
//  TeacherNormalCRV.m
//  jz_sanrenxing
//
//  Created by Killua on 2016/10/4.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "TeacherNormalCRV.h"

@implementation TeacherNormalCRV

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        UILabel *textLabel = [[UILabel alloc] init];
        textLabel.text = @"推荐";
        [self addSubview:textLabel];

        [textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self).with.offset(12);
            make.centerY.mas_equalTo(self);
        }];

    }
    return self;
}

@end
