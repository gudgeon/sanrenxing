//
//  TeacherHotCRV.m
//  jz_sanrenxing
//
//  Created by Killua on 2016/10/4.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "TeacherHotCRV.h"

@implementation TeacherHotCRV

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // title label
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.text = @"正在众筹";
        [self addSubview:titleLabel];

        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(10);
        }];

        // detail button
        UIButton *detailButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [detailButton setTitle:@"热门" forState:UIControlStateNormal];
        detailButton.titleLabel.font = [UIFont systemFontOfSize:13.f];
        [detailButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [detailButton setImage:[UIImage imageNamed:@"gengduo_index"] forState:UIControlStateNormal];
        [self addSubview:detailButton];

        [detailButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(titleLabel);
            make.bottom.mas_equalTo(self).with.offset(-10);
        }];

    }
    return self;
}

@end
