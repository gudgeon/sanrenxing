//
//  TeacherHomeCVL.m
//  jz_sanrenxing
//
//  Created by Killua on 2016/10/4.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "TeacherHomeCVL.h"

@implementation TeacherHomeCVL

+ (instancetype)teacherInfoLayout {
    UIImage *img      = [UIImage imageNamed:@"login_img"];
    CGFloat imgWidth  = (SCREEN_WIDTH - 12 * 4) / 3;
    CGFloat imgHeight = imgWidth * (img.size.height / img.size.width);

    CGFloat downViewHeight = 70;
    CGFloat cellWidth      = imgWidth;
    CGFloat cellHeight     = imgHeight + downViewHeight;

    TeacherHomeCVL *layout = [[TeacherHomeCVL alloc] init];
    layout.minimumInteritemSpacing = 12;
    layout.minimumLineSpacing = 12;
    layout.itemSize = CGSizeMake(cellWidth,cellHeight);
    layout.sectionInset = UIEdgeInsetsMake(0, 12, 20, 12);
    layout.headerReferenceSize = CGSizeMake(SCREEN_WIDTH, 38);

    return layout;
}

@end
