//
//  TeacherHomeCVL.h
//  jz_sanrenxing
//
//  Created by Killua on 2016/10/4.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeacherHomeCVL : UICollectionViewFlowLayout


/**
 教师列表布局

 @return Layout
 */
+ (instancetype)teacherInfoLayout;

@end
