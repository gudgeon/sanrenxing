//
//  TeacherCollectionViewCell.m
//  jz_sanrenxing
//
//  Created by GuJun on 16/9/30.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "TeacherCollectionViewCell.h"

@implementation TeacherCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // 初始化时加载collectionCell.xib文件
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"TeacherCollectionViewCell" owner:self options:nil];
        
        // 如果路径不存在，return nil
        if (arrayOfViews.count < 1)
        {
            return nil;
        }
        // 如果xib中view不属于UICollectionViewCell类，return nil
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]])
        {
            return nil;
        }
        // 加载nib
        self = [arrayOfViews objectAtIndex:0];
    }
    return self;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (void)setRaisemodel:(JZRaiseModel *)Raisemodel{
    
}

- (void)setRaiseUserDetailModel:(JZRaiseUserDetailModel *)raiseUserDetailModel{
    
    
    self.teacherName.text = raiseUserDetailModel.user_name;
    
    self.Introduction.text = raiseUserDetailModel.user_brief;
    
    [self.teacherIcon sd_setImageWithURL:[NSURL URLWithString:raiseUserDetailModel.user_avatar]];
    
}

@end
