//
//  JZRaiseModel.h
//  jz_sanrenxing
//
//  Created by GuJun on 16/10/8.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JZRaiseUserDetailModel.h"

@interface JZRaiseModel : NSObject

/** 个推的CID */
@property (nonatomic, strong) NSString  * client_cid;

/** 客户端imie码 */
@property (nonatomic, strong) NSString  * client_imei;

/** 客户端类型1为android，2为ios */
@property (nonatomic, assign) int         client_type;

/** 邀请码 */
@property (nonatomic, strong) NSString  * invitation_code;

/** 用户邮箱 */
@property (nonatomic, strong) NSString  * user_email;

/** 用户ID */
@property (nonatomic, strong) NSString  * user_id;

/** 用户昵称 */
@property (nonatomic, strong) NSString  * user_nickname;

/** 用户密码 */
@property (nonatomic, strong) NSString  * user_password;

/** 用户电话 */
@property (nonatomic, strong) NSString  * user_phone;

/** 用户角色 1普通用户 2可开课的用户 */
@property (nonatomic, assign) int         user_role;

/** 用户token */
@property (nonatomic, strong) NSString  * user_token;

/** 用户JZRaiseUserDetailModel*/
@property (nonatomic, strong) JZRaiseUserDetailModel   * userDetail;

/** 1表示本系统账户，2表示第三方账户 */
@property (nonatomic, assign) int         user_type;

/** 添加时间 */
@property (nonatomic, strong) NSString  * add_time;

/** 是否删除 1已删除，2未删除 */
@property (nonatomic, assign) int         is_delete;


@end
