//
//  JZRaiseUserDetailModel.h
//  jz_sanrenxing
//
//  Created by GuJun on 16/10/8.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JZRaiseUserDetailModel : NSObject

/** 添加时间 */
@property (nonatomic, strong) NSString  * add_time;

/** 是否删除 1已删除，2未删除 */
@property (nonatomic, assign) int         is_delete;

/** 是否热门1=热门，2=非热门 */
@property (nonatomic, assign) int       * is_hot;

/** 是否推荐1=推荐，2=非推荐 */
@property (nonatomic, assign) int       * is_recommend;

/** 用户头像 */
@property (nonatomic, strong) NSString  * user_avatar;

/** 用户出生日期 */
@property (nonatomic, strong) NSString  * user_birthday;

/** 用户简介 */
@property (nonatomic, strong) NSString  * user_brief;

/** 用户职业 */
@property (nonatomic, strong) NSString  * user_careers;

/** 用户详情ID */
@property (nonatomic, strong) NSString  * user_detail_id;

/** 1表示男，2表示女，3表示保密 */
@property (nonatomic, assign) int         user_gender;

/** 用户真实姓名 */
@property (nonatomic, strong) NSString  * user_name;

/** 用户金币 */
@property (nonatomic, strong) NSString  * user_money;

/** 用户总积分 */
@property (nonatomic, assign) int         user_integral;

/** 用户ID */
@property (nonatomic, strong) NSString  * user_id;



@end
