//
//  JZCourseModel.h
//  jz_sanrenxing
//
//  Created by GuJun on 16/10/5.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JZCourseModel : NSObject

/** 课程id */
@property (nonatomic, strong) NSString * course_id;

/** typeID */
@property (nonatomic, assign) int  type_id;

/** 课程名称 */
@property (nonatomic, strong) NSString * course_name;

/** img */
@property (nonatomic, strong) NSArray * imgs;

/** 费用 */
@property (nonatomic, assign) double course_pay;

/** 学习人数 */
@property (nonatomic, assign) int course_study_num;

/** 章节总数 */
@property (nonatomic, assign) int course_part_num;

/** 课程性质 */
@property (nonatomic, assign) int course_property;

/** 课程简介 */
@property (nonatomic, strong) NSString * course_brief;

/** 课程关键字 */
@property (nonatomic, strong) NSString * course_keyword;

/** 是否热门1=热门，2=非热门 */
@property (nonatomic, assign) int is_hot;

/** 是否推荐1=推荐，2=非推荐 */
@property (nonatomic, assign) int is_recommend;

/** 课程状态 */
@property (nonatomic, assign) int course_status;

/** 添加时间 */
@property (nonatomic, strong) NSString * add_time;

/** 是否删除 1已删除，2未删除 */
@property (nonatomic, assign) int is_delete;



@end
