//
//  JZCapterModel.h
//  jz_sanrenxing
//
//  Created by GuJun on 16/10/12.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JZCapterModel : NSObject

/** 章节名称 */
@property (nonatomic, strong) NSString * part_name;

/** 是否删除 */
@property (nonatomic, assign) int  is_delete;

/** 添加时间 */
@property (nonatomic, strong) NSString * add_time;

/** 课程id  */
@property (nonatomic, strong) NSString * course_id;

/** 课程简介 */
@property (nonatomic, assign) NSString * part_brief;

/** 章节id */
@property (nonatomic, strong) NSString * part_id;

/** 章节关键字  */
@property (nonatomic, strong) NSString * part_keyword;

/** 视频url */
@property (nonatomic, assign) NSString * part_url;


@end
