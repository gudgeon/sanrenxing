//
//  JZCurrentUserModel.m
//  jz_sanrenxing
//
//  Created by GuJun on 16/9/29.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "JZCurrentUserModel.h"

#define KEY_USER_ID @"keyUserId"
#define KEY_USER_PHONE @"keyUserPhone"
#define KEY_USER_PASSWORD @"keyUserPassword"
#define KEY_USER_INVITATIONCODE @"keyUserInvitationCode"
#define KEY_USER_USER_ROLE @"keyUserRole"
#define KEY_USER_TOKEN @"keyUserToken"
#define KEY_USER_TYPE @"keyUserType"
#define KEY_USER_CLIENT_IMEI @"keyUserClientIMEI"
#define KEY_USER_NICK_NAME @"keyUserNickName"
#define KEY_USER_EMAIL @"keyUserEmail"
#define KEY_USER_CLIENT_CID @"keyUserClientCid"
#define KEY_USER_USERSIG    @"keyUserUserSig"
#define KEY_USER_DETAIL_ID @"keyUserDetailId"
#define KEY_USER_NAME @"keyUserName"
#define KEY_USER_AVATAR @"keyUserAvatar"
#define KEY_USER_GENDER @"keyUserGender"
#define KEY_USER_BIRTHDAY @"keyUserBirthday"
#define KEY_USER_BRIEF @"keyUserBrief"
#define KEY_USER_CAREERS @"keyUserCareers"
#define KEY_USER_INTEGRAL @"keyUserIntegral"
#define KEY_USER_MONEY @"keyUserMoney"
#define KEY_USER_ISHOT @"keyUserIsHot"
#define KEY_USER_RECOMMEND @"keyUserRecommend"

@implementation JZCurrentUserModel

@synthesize userId = _userId;
@synthesize phone = _phone;
@synthesize password = _password;
@synthesize invitationCode = _invitationCode;
@synthesize userRole = _userRole;
@synthesize userToken = _userToken;
@synthesize userType = _userType;
@synthesize clientIMEI = _clientIMEI;
@synthesize userNickname = _userNickname;
@synthesize userEmail = _userEmail;
@synthesize clientCid = _clientCid;
@synthesize userSig   = _userSig;
@synthesize userDetailId = _userDetailId;
@synthesize userName = _userName;
@synthesize userAvatar = _userAvatar;
@synthesize userGender = _userGender;
@synthesize userBirthday = _userBirthday;
@synthesize userBrief = _userBrief;
@synthesize userCareers = _userCareers;
@synthesize userIntegral = _userIntegral;
@synthesize userMoney = _userMoney;
@synthesize isHot = _isHot;
@synthesize isRecommend = _isRecommend;


SingletonM(JZCurrentUserModel)

- (NSString *)userId {
    if (!_userId) {
        _userId = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_ID];
    }
    return _userId;
}

- (void)setUserId:(NSString *)userId {
    _userId = userId;
    [[NSUserDefaults standardUserDefaults] setObject:_userId forKey:KEY_USER_ID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)phone {
    if (!_phone) {
        _phone = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_PHONE];
    }
    return _phone;
}

- (void)setPhone:(NSString *)phone {
    _phone = phone;
    [[NSUserDefaults standardUserDefaults] setObject:_phone forKey:KEY_USER_PHONE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)password {
    if (!_password) {
        _password = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_PASSWORD];
    }
    return _password;
}

- (void)setPassword:(NSString *)password {
    _password = password;
    [[NSUserDefaults standardUserDefaults] setObject:_password forKey:KEY_USER_PASSWORD];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)invitationCode {
    if (!_invitationCode) {
        _invitationCode = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_INVITATIONCODE];
    }
    return _invitationCode;
}

- (void)setInvitationCode:(NSString *)invitationCode {
    _invitationCode = invitationCode;
    [[NSUserDefaults standardUserDefaults] setObject:_invitationCode forKey:KEY_USER_INVITATIONCODE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)userRole {
    if (!_userRole) {
        _userRole = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_USER_ROLE];
    }
    return _userRole;
}

- (void)setUserRole:(NSString *)userRole {
    _userRole = userRole;
    [[NSUserDefaults standardUserDefaults] setObject:_userRole forKey:KEY_USER_USER_ROLE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)userToken {
    if (!_userToken) {
        _userToken = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_TOKEN];
    }
    return _userToken;
}

- (void)setUserToken:(NSString *)userToken {
    _userToken = userToken;
    [[NSUserDefaults standardUserDefaults] setObject:_userToken forKey:KEY_USER_TOKEN];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)userType {
    if (!_userType) {
        _userType = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_TYPE];
    }
    return _userType;
}

- (void)setUserType:(NSString *)userType {
    _userType = userType;
    [[NSUserDefaults standardUserDefaults] setObject:_userType forKey:KEY_USER_TYPE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)clientIMEI {
    if (!_clientIMEI) {
        _clientIMEI = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_CLIENT_IMEI];
    }
    return _clientIMEI;
}

- (void)setClientIMEI:(NSString *)clientIMEI {
    _clientIMEI = clientIMEI;
    [[NSUserDefaults standardUserDefaults] setObject:_clientIMEI forKey:KEY_USER_CLIENT_IMEI];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)userNickname {
    if (!_userNickname) {
        _userNickname = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_NICK_NAME];
    }
    return _userNickname;
}

- (void)setUserNickname:(NSString *)userNickname {
    _userNickname = userNickname;
    [[NSUserDefaults standardUserDefaults] setObject:_userNickname forKey:KEY_USER_NICK_NAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)userEmail {
    if (!_userEmail) {
        _userEmail = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_EMAIL];
    }
    return _userEmail;
}

- (void)setUserEmail:(NSString *)userEmail {
    _userEmail = userEmail;
    [[NSUserDefaults standardUserDefaults] setObject:_userEmail forKey:KEY_USER_EMAIL];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)clientCid {
    if (!_clientCid) {
        _clientCid = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_CLIENT_CID];
    }
    return _clientCid;
}

- (void)setClientCid:(NSString *)clientCid {
    _clientCid = clientCid;
    [[NSUserDefaults standardUserDefaults] setObject:_clientCid forKey:KEY_USER_CLIENT_CID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString *)userSig{
    if (!_userSig) {
        _userSig = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_USERSIG];
    }
    return _userSig;
}

- (void)setUserSig:(NSString *)userSig{
    _userSig = userSig;
    [[NSUserDefaults standardUserDefaults] setObject:_userSig forKey:KEY_USER_USERSIG];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)userDetailId {
    if (!_userDetailId) {
        _userDetailId = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_DETAIL_ID];
    }
    return _userDetailId;
}

- (void)setUserDetailId:(NSString *)userDetailId {
    _userDetailId = userDetailId;
    [[NSUserDefaults standardUserDefaults] setObject:_userDetailId forKey:KEY_USER_DETAIL_ID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)userName {
    if (!_userName) {
        _userName = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_NAME];
    }
    return _userName;
}

- (void)setUserName:(NSString *)userName {
    _userName = userName;
    [[NSUserDefaults standardUserDefaults] setObject:_userName forKey:KEY_USER_NAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)userAvatar {
    if (!_userAvatar) {
        _userAvatar = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_AVATAR];
    }
    return _userAvatar;
}

- (void)setUserAvatar:(NSString *)userAvatar {
    _userAvatar = userAvatar;
    [[NSUserDefaults standardUserDefaults] setObject:_userAvatar forKey:KEY_USER_AVATAR];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)userGender {
    if (!_userGender) {
        _userGender = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_GENDER];
    }
    return _userGender;
}

- (void)setUserGender:(NSString *)userGender {
    _userGender = userGender;
    [[NSUserDefaults standardUserDefaults] setObject:_userGender forKey:KEY_USER_GENDER];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)userBirthday {
    if (!_userBirthday) {
        _userBirthday = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_BIRTHDAY];
    }
    return _userBirthday;
}

- (void)setUserBirthday:(NSString *)userBirthday {
    _userBirthday = userBirthday;
    [[NSUserDefaults standardUserDefaults] setObject:_userBirthday forKey:KEY_USER_BIRTHDAY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)userBrief {
    if (!_userBrief) {
        _userBrief = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_BRIEF];
    }
    return _userBrief;
}

- (void)setUserBrief:(NSString *)userBrief {
    _userBrief = userBrief;
    [[NSUserDefaults standardUserDefaults] setObject:_userBrief forKey:KEY_USER_BRIEF];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)userCareers {
    if (!_userCareers) {
        _userCareers = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_CAREERS];
    }
    return _userCareers;
}

- (void)setUserCareers:(NSString *)userCareers {
    _userCareers = userCareers;
    [[NSUserDefaults standardUserDefaults] setObject:_userCareers forKey:KEY_USER_CAREERS];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)userIntegral {
    if (!_userIntegral) {
        _userIntegral = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_INTEGRAL];
    }
    return _userIntegral;
}

- (void)setUserIntegral:(NSString *)userIntegral {
    _userIntegral = userIntegral;
    [[NSUserDefaults standardUserDefaults] setObject:_userIntegral forKey:KEY_USER_INTEGRAL];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)userMoney {
    if (!_userMoney) {
        _userMoney = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_MONEY];
    }
    return _userMoney;
}

- (void)setUserMoney:(NSString *)userMoney{
    _userMoney = userMoney;
    [[NSUserDefaults standardUserDefaults] setObject:_userMoney forKey:KEY_USER_MONEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)isHot {
    if (!_isHot) {
        _isHot = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_ISHOT];
    }
    return _isHot;
}

- (void)setIsHot:(NSString *)isHot {
    _isHot = isHot;
    [[NSUserDefaults standardUserDefaults] setObject:_isHot forKey:KEY_USER_ISHOT];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)isRecommend {
    if (!_isRecommend) {
        _isRecommend = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_RECOMMEND];
    }
    return _isRecommend;
}

- (void)setIsRecommend:(NSString *)isRecommend {
    _isRecommend = isRecommend;
    [[NSUserDefaults standardUserDefaults] setObject:_isRecommend forKey:KEY_USER_RECOMMEND];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)saveUserInfoWithDictionary:(NSDictionary *)dict {
    self.userId = judgeStr(dict[@"user_id"]);
    self.phone = judgeStr(dict[@"user_phone"]);
    self.password = judgeStr(dict[@"user_password"]);
    self.invitationCode = judgeStr(dict[@"invitation_code"]);
    self.userRole = judgeStr(dict[@"user_role"]);
    self.userToken = judgeStr(dict[@"user_token"]);
    self.userType = judgeStr(dict[@"user_type"]);
    self.clientIMEI = judgeStr(dict[@"client_imei"]);
    self.userNickname = judgeStr(dict[@"user_nickname"]);
    self.userEmail = judgeStr(dict[@"user_email"]);
    self.clientCid = judgeStr(dict[@"client_cid"]);
    NSDictionary *detailDict = dict[@"userDetail"];
    self.userSig   = judgeStr(dict[@"userSig"]);
    self.userDetailId = judgeStr(detailDict[@"user_detail_id"]);
    self.userName = judgeStr(detailDict[@"user_name"]);
    self.userAvatar = judgeStr(detailDict[@"user_avatar"]);
    self.userGender = judgeStr(detailDict[@"user_gender"]);
    self.userBirthday = judgeStr(detailDict[@"user_birthday"]);
    self.userBrief = judgeStr(detailDict[@"user_brief"]);
    self.userCareers = judgeStr(detailDict[@"user_careers"]);
    self.userIntegral = judgeStr(detailDict[@"user_integral"]);
    self.userMoney = judgeStr(detailDict[@"user_money"]);
    self.isHot = judgeStr(detailDict[@"is_hot"]);
    self.isRecommend = judgeStr(detailDict[@"is_recommend"]);
}

- (void)cleanUserInfo {
    self.userId = @"";
    self.phone = @"";
    self.password = @"";
    self.invitationCode = @"";
    self.userRole = @"";
    self.userToken = @"";
    self.userType = @"";
    self.clientIMEI = @"";
    self.userNickname = @"";
    self.userEmail = @"";
    self.clientCid = @"";
    self.userSig   = @"";
    self.userDetailId = @"";
    self.userName = @"";
    self.userAvatar = @"";
    self.userGender = @"";
    self.userBirthday = @"";
    self.userBrief = @"";
    self.userCareers = @"";
    self.userIntegral = @"";
    self.userMoney = @"";
    self.isHot = @"";
    self.isRecommend = @"";
}

- (BOOL)isLogin {
    if (!self.userToken) {
        self.userToken = @"";
    }
    return ![self.userToken isEqualToString:@""];
}

@end
