//
//  JZImageModel.h
//  jz_sanrenxing
//
//  Created by GuJun on 16/10/5.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JZImageModel : NSObject

/** 图片简介 */
@property (nonatomic, strong) NSString * img_brief;

/** 图片Id */
@property (nonatomic, strong) NSString * img_id;

/** pid（图片所属） */
@property (nonatomic, assign) int pid;

/** 图片的url */
@property (nonatomic, strong) NSString * img_url;



@end
