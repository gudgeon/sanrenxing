//
//  JZCurrentUserModel.h
//  jz_sanrenxing
//
//  Created by GuJun on 16/9/29.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SingletonMacro.h"

@interface JZCurrentUserModel : NSObject

@property (copy, nonatomic) NSString *userId;
@property (copy, nonatomic) NSString *phone;
@property (copy, nonatomic) NSString *password;
@property (copy, nonatomic) NSString *invitationCode;
@property (copy, nonatomic) NSString *userRole;
@property (copy, nonatomic) NSString *userToken;
@property (copy, nonatomic) NSString *userType;
@property (copy, nonatomic) NSString *clientIMEI;
@property (copy, nonatomic) NSString *userNickname;
@property (copy, nonatomic) NSString *userEmail;
@property (copy, nonatomic) NSString *clientCid;
@property (copy, nonatomic) NSString *userSig;
@property (copy, nonatomic) NSString *userDetailId;
@property (copy, nonatomic) NSString *userName;
@property (copy, nonatomic) NSString *userAvatar;
@property (copy, nonatomic) NSString *userGender;
@property (copy, nonatomic) NSString *userBirthday;
@property (copy, nonatomic) NSString *userBrief;
@property (copy, nonatomic) NSString *userCareers;
@property (copy, nonatomic) NSString *userIntegral;
@property (copy, nonatomic) NSString *userMoney;
@property (copy, nonatomic) NSString *isHot;
@property (copy, nonatomic) NSString *isRecommend;

SingletonH(JZCurrentUserModel)


/**
 保存登录用户信息

 @param dict 用户信息
 */
- (void)saveUserInfoWithDictionary:(NSDictionary *)dict;


/**
 清楚用户信息
 */
- (void)cleanUserInfo;


/**
 检测用户是否登录

 @return YES / NO
 */
- (BOOL)isLogin;

@end
