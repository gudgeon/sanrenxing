//
//  UserInfoViewController.m
//  jz_sanrenxing
//
//  Created by Killua on 2016/10/6.
//  Copyright © 2016年 杭州景忠网络科技有限公司. All rights reserved.
//

#import "UserInfoViewController.h"
#import "InputTableViewCell.h"
#import "UserInfoTableView.h"
#import "JZCurrentUserModel.h"
#import "ImageSelectManager.h"
#import <UIImageView+WebCache.h>

@interface UserInfoViewController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UserInfoTableViewDelegate, ImageSelectDelegate>
{
    UITextField *_nickNameTextField;
    NSString *_imagePath;
}
@property (weak, nonatomic) IBOutlet UserInfoTableView *tableview;
@property (strong, nonatomic) ImageSelectManager *imageSelectManager;

@property (strong, nonatomic) IBOutlet UIView *headView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;

- (IBAction)logout:(UIButton *)sender;
@end

@implementation UserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self initUI];
    _imageSelectManager = [[ImageSelectManager alloc] init];
    _imageSelectManager.delegate = self;
    NSString *avatar = [JZCurrentUserModel sharedJZCurrentUserModel].userAvatar;
    [_avatarImageView sd_setImageWithURL:[NSURL URLWithString:avatar] placeholderImage:[UIImage imageWithContentsOfFile:@"login_img"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initUI {
    _tableview.touchDelegate = self;
    _tableview.tableHeaderView = _headView;
    _tableview.tableFooterView = [[UIView alloc] init];
    _tableview.tableFooterView = _footerView;
    _avatarImageView.layer.cornerRadius = 40;
    _avatarImageView.layer.masksToBounds = YES;
    _logoutButton.layer.cornerRadius = 4;
    _logoutButton.layer.masksToBounds = YES;

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addPicture)];
    [_avatarImageView addGestureRecognizer:tap];


    UIButton *saveButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    [saveButton setTitle:@"保存" forState:UIControlStateNormal];
    [saveButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    saveButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [saveButton addTarget:self action:@selector(saveUserInfo) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:saveButton];
    self.navigationItem.rightBarButtonItem = item;
}

- (void)addPicture {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"修改头像" message:@"请选择以下方式添加图片" preferredStyle:UIAlertControllerStyleActionSheet];

    __weak typeof(self) weakSelf = self;
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf.imageSelectManager pickImageFromCamera:YES];
    }];

    UIAlertAction *albumAction = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf.imageSelectManager pickImageFromAlbum:YES];
    }];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];

    [controller addAction:cameraAction];
    [controller addAction:albumAction];
    [controller addAction:cancelAction];

    [self presentViewController:controller animated:YES completion:^{
    }];
}

- (void)saveUserInfo {
    NSURL *fileUrl;
    if (_imagePath) {
        fileUrl = [NSURL fileURLWithPath:_imagePath];
    }
    [HttpUtils requestToUpdateUserInfoWithNickName:_nickNameTextField.text email:nil phone:nil name:nil fileURL:fileUrl gender:nil birthday:nil brief:nil careers:nil callback:^(int code, NSDictionary *dict, NSString *msg) {
        if (1 == code) {
            [SVProgressHUD showSuccessWithStatus:msg];
            [[JZCurrentUserModel sharedJZCurrentUserModel] saveUserInfoWithDictionary:dict];
            [self.navigationController popToRootViewControllerAnimated:YES];
        } else {
            [SVProgressHUD showInfoWithStatus:msg];
        }
    }];
}


#pragma mark - UITableViewDelegate && UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    InputTableViewCell *cell = [InputTableViewCell cellWithTableView:tableView];
    cell.inputTextField.delegate = self;
    _nickNameTextField = cell.inputTextField;

    return cell;
}

- (void)touchTableView {
    [_nickNameTextField resignFirstResponder];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark ImageSelectDelegate
- (void)selectedImage:(UIImage *)image FullPath:(NSString *)fullPath VideoPath:(NSString *)videoPath{
    if (!image) { return; }

    _imagePath = fullPath;
    _avatarImageView.image = [UIImage imageWithContentsOfFile:fullPath];
}

- (IBAction)logout:(UIButton *)sender {
    [[JZCurrentUserModel sharedJZCurrentUserModel] cleanUserInfo];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
